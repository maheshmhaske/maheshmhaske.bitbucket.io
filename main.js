(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/account/account-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/account/account-routing.module.ts ***!
  \***************************************************/
/*! exports provided: AccountRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountRoutingModule", function() { return AccountRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../team-list.resolver */ "./src/app/team-list.resolver.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/account/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/account/signup/signup.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var accountRoutes = [
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    },
    {
        path: 'signup',
        component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_4__["SignupComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    }
];
var AccountRoutingModule = /** @class */ (function () {
    function AccountRoutingModule() {
    }
    AccountRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(accountRoutes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AccountRoutingModule);
    return AccountRoutingModule;
}());



/***/ }),

/***/ "./src/app/account/account.module.ts":
/*!*******************************************!*\
  !*** ./src/app/account/account.module.ts ***!
  \*******************************************/
/*! exports provided: AccountModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountModule", function() { return AccountModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/login.component */ "./src/app/account/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/account/signup/signup.component.ts");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _account_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./account-routing.module */ "./src/app/account/account-routing.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AccountModule = /** @class */ (function () {
    function AccountModule() {
    }
    AccountModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _account_routing_module__WEBPACK_IMPORTED_MODULE_6__["AccountRoutingModule"]
            ],
            declarations: [_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"], _signup_signup_component__WEBPACK_IMPORTED_MODULE_3__["SignupComponent"]]
        })
    ], AccountModule);
    return AccountModule;
}());



/***/ }),

/***/ "./src/app/account/login/login.component.html":
/*!****************************************************!*\
  !*** ./src/app/account/login/login.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"page-wrap mb-5\">\r\n    <div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10 offset-xl-2 col-xl-8\">\r\n\t      \t\t<div class=\"login-wrap\">\r\n\t      \t\t\t<div class=\"row\">\r\n\t\t      \t\t\t<div class=\"col-12 col-sm-6 col-md-4 col-lg-5 col-xl-5\">\r\n\t\t      \t\t\t\t<div class=\"box-wrap h-100 left-wrap-bg\">\r\n\t\t      \t\t\t\t\t<h1>Login</h1>\r\n\t\t      \t\t\t\t\t<p>Get access to your Orders, Wishlist and Recommendations</p>\r\n\t\t      \t\t\t\t\t<div class=\"texture p-2\">\r\n\t\t\t\t\t\t          \t<img alt=\"Title\" src=\"./assets/img/title-icon.png\" class=\"\">\r\n\t\t\t\t\t\t        </div>\r\n\t\t      \t\t\t\t</div>\r\n\t\t      \t\t\t</div>\r\n\t\t      \t\t\t<div class=\"col-12 col-sm-6 col-md-8 col-lg-7 col-xl-7\">\r\n\t\t      \t\t\t\t<div class=\"box-wrap h-100\">\r\n\t\t\t      \t\t\t\t<form class=\"login-form\" [formGroup]=\"userLoginValidations\" (ngSubmit)=\"userLoginValidations.valid && login()\" novalidate>\r\n\t\t\t\t\t\t\t\t\t<mat-form-field class=\"example-full-width d-block\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-label>Your E-mail Id</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t<input matInput formControlName=\"email\" placeholder=\"Your E-mail Id\" required>\r\n\t\t\t\t\t\t\t\t\t\t<mat-error *ngIf=\"userLoginValidations.get('email').hasError('required')\">\r\n\t\t\t\t\t\t\t\t\t      \tE-mail is Required!\r\n\t\t\t\t\t\t\t\t\t    </mat-error>\r\n\t\t\t\t\t\t\t\t\t    <mat-error *ngIf=\"userLoginValidations.get('email').hasError('pattern')\">\r\n\t\t\t\t\t\t\t\t\t      \tPlease enter a valid email address\r\n\t\t\t\t\t\t\t\t\t    </mat-error>\r\n\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t<mat-form-field class=\"example-full-width  d-block\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-label>Password</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t<input matInput type=\"{{showPassword ? '' : 'password'}}\" formControlName=\"passowrd\" placeholder=\"Password\" required>\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-eye\" aria-hidden=\"true\" (click)=\"showPassword = true\" *ngIf=\"!showPassword\"></i>\r\n\t\t\t\t            \t\t\t<i class=\"fa fa-eye-slash\" aria-hidden=\"true\" (click)=\"showPassword = false\" *ngIf=\"showPassword\"></i>\r\n\t\t\t\t            \t\t\t\r\n\t\t\t\t\t\t\t\t\t    <mat-error *ngIf=\"userLoginValidations.get('passowrd').hasError('required')\">\r\n\t\t\t\t\t\t\t\t\t      \tPassowrd is Required!\r\n\t\t\t\t\t\t\t\t\t    </mat-error>\r\n\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t<div class=\"form-group text-center mt-3\">\r\n\t\t\t\t\t\t\t\t    \t<button  type=\"submit\" class=\"btn btn-login\">Login</button>\r\n\t\t\t\t\t\t\t\t    </div>\r\n\t\t\t\t\t\t\t\t</form>\r\n\r\n\t\t\t\t\t\t\t\t<p class=\"ca\"><a routerLink=\"/signup\" routerLinkActive=\"active\" >New to Site? Create an account</a></p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t      \t\t\t</div>\r\n\t\t      \t\t</div>\r\n\t      \t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</section>"

/***/ }),

/***/ "./src/app/account/login/login.component.scss":
/*!****************************************************!*\
  !*** ./src/app/account/login/login.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-wrap {\n  box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n  margin: 60px 0; }\n  .login-wrap .fa-eye, .login-wrap .fa-eye-slash {\n    float: right;\n    margin-top: -16px; }\n  .login-wrap .btn-login {\n    background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n    margin-right: 20px;\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Baskerville\", serif;\n    padding: 14px 40px;\n    border-radius: 0;\n    border-top: 2px solid transparent; }\n  .login-wrap .btn-login:hover, .login-wrap .btn-login:focus {\n      box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.6);\n      border-top: 2px solid #c6755d; }\n  .login-wrap .box-wrap {\n    padding: 40px 35px;\n    font-size: 15px; }\n  .login-wrap .box-wrap.left-wrap-bg {\n      background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n      color: #444; }\n  .login-wrap .box-wrap h1 {\n      font-weight: 400;\n      font-family: \"Libre Baskerville\", serif;\n      color: #000;\n      font-size: 36px; }\n  .login-wrap .box-wrap .ca {\n      text-align: center;\n      padding-top: 10px; }\n  .login-wrap .box-wrap .ca a {\n        color: #2874f0; }\n  .login-wrap .box-wrap .texture {\n      position: absolute;\n      left: 0;\n      right: 0;\n      bottom: 30px;\n      text-align: center; }\n  @media (max-width: 767px) {\n        .login-wrap .box-wrap .texture {\n          bottom: 10px; } }\n"

/***/ }),

/***/ "./src/app/account/login/login.component.ts":
/*!**************************************************!*\
  !*** ./src/app/account/login/login.component.ts ***!
  \**************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder) {
        this.formBuilder = formBuilder;
        this.showPassword = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.userLoginValidations = this.formBuilder.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            passowrd: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]]
        });
    };
    LoginComponent.prototype.login = function () {
        console.log(this.userLoginValidations);
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/account/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/account/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/account/signup/signup.component.html":
/*!******************************************************!*\
  !*** ./src/app/account/signup/signup.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"page-wrap mb-5\">\r\n    <div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10 offset-xl-2 col-xl-8\">\r\n\t      \t\t<div class=\"signup-wrap\">\r\n\t      \t\t\t<div class=\"row\">\r\n\t\t      \t\t\t<div class=\"col-12 col-sm-6 col-md-4 col-lg-5 col-xl-5\">\r\n\t\t      \t\t\t\t<div class=\"box-wrap h-100 left-wrap-bg\">\r\n\t\t      \t\t\t\t\t<h1>Signup</h1>\r\n\t\t      \t\t\t\t\t<p>We do not share your personal details with anyone.</p>\r\n\t\t      \t\t\t\t\t<div class=\"texture p-2\">\r\n\t\t\t\t\t\t          \t<img alt=\"Title\" src=\"./assets/img/title-icon.png\" class=\"\">\r\n\t\t\t\t\t\t        </div>\r\n\t\t      \t\t\t\t</div>\r\n\t\t      \t\t\t</div>\r\n\t\t      \t\t\t<div class=\"col-12 col-sm-6 col-md-8 col-lg-7 col-xl-7\">\r\n\t\t      \t\t\t\t<div class=\"box-wrap h-100\">\r\n\t\t\t      \t\t\t\t<form class=\"login-form\" [formGroup]=\"userSignupValidations\" (ngSubmit)=\"userSignupValidations.valid && signup()\" novalidate>\r\n\t\t\t      \t\t\t\t\t<mat-form-field class=\"example-full-width  d-block\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-label>Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t<input matInput  formControlName=\"name\" placeholder=\"Password\" required>\r\n\t\t\t\t\t\t\t\t\t    <mat-error *ngIf=\"userSignupValidations.get('passowrd').hasError('required')\">\r\n\t\t\t\t\t\t\t\t\t      \tPassowrd is Required!\r\n\t\t\t\t\t\t\t\t\t    </mat-error>\r\n\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t<mat-form-field class=\"example-full-width d-block\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-label>Your E-mail Id</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t<input matInput formControlName=\"email\" placeholder=\"Your E-mail Id\" required>\r\n\t\t\t\t\t\t\t\t\t\t<mat-error *ngIf=\"userSignupValidations.get('email').hasError('required')\">\r\n\t\t\t\t\t\t\t\t\t      \tE-mail is Required!\r\n\t\t\t\t\t\t\t\t\t    </mat-error>\r\n\t\t\t\t\t\t\t\t\t    <mat-error *ngIf=\"userSignupValidations.get('email').hasError('pattern')\">\r\n\t\t\t\t\t\t\t\t\t      \tPlease enter a valid email address\r\n\t\t\t\t\t\t\t\t\t    </mat-error>\r\n\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t<mat-form-field class=\"example-full-width  d-block\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-label>Password</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t<input matInput type=\"{{showPassword ? '' : 'password'}}\" formControlName=\"passowrd\" placeholder=\"Password\" required>\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-eye\" aria-hidden=\"true\" (click)=\"showPassword = true\" *ngIf=\"!showPassword\"></i>\r\n\t\t\t\t            \t\t\t<i class=\"fa fa-eye-slash\" aria-hidden=\"true\" (click)=\"showPassword = false\" *ngIf=\"showPassword\"></i>\r\n\t\t\t\t            \t\t\t\r\n\t\t\t\t\t\t\t\t\t    <mat-error *ngIf=\"userSignupValidations.get('passowrd').hasError('required')\">\r\n\t\t\t\t\t\t\t\t\t      \tPassowrd is Required!\r\n\t\t\t\t\t\t\t\t\t    </mat-error>\r\n\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t<div class=\"form-group text-center mt-3\">\r\n\t\t\t\t\t\t\t\t    \t<button  type=\"submit\" class=\"btn btn-signup\">Signup</button>\r\n\t\t\t\t\t\t\t\t    </div>\r\n\t\t\t\t\t\t\t\t</form>\r\n\r\n\t\t\t\t\t\t\t\t<p class=\"ca\"><a routerLink=\"/login\" routerLinkActive=\"active\" >Existing User? Log in</a></p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t      \t\t\t</div>\r\n\t\t      \t\t</div>\r\n\t      \t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</section>"

/***/ }),

/***/ "./src/app/account/signup/signup.component.scss":
/*!******************************************************!*\
  !*** ./src/app/account/signup/signup.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".signup-wrap {\n  box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n  margin: 60px 0; }\n  .signup-wrap .fa-eye, .signup-wrap .fa-eye-slash {\n    float: right;\n    margin-top: -16px; }\n  .signup-wrap .btn-signup {\n    background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n    margin-right: 20px;\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Baskerville\", serif;\n    padding: 14px 40px;\n    border-radius: 0;\n    border-top: 2px solid transparent; }\n  .signup-wrap .btn-signup:hover, .signup-wrap .btn-signup:focus {\n      box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.6);\n      border-top: 2px solid #c6755d; }\n  .signup-wrap .box-wrap {\n    padding: 40px 35px;\n    font-size: 15px; }\n  .signup-wrap .box-wrap.left-wrap-bg {\n      background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n      color: #444; }\n  .signup-wrap .box-wrap h1 {\n      font-weight: 400;\n      font-family: \"Libre Baskerville\", serif;\n      color: #000;\n      font-size: 36px; }\n  .signup-wrap .box-wrap .ca {\n      text-align: center;\n      padding-top: 10px; }\n  .signup-wrap .box-wrap .ca a {\n        color: #2874f0; }\n  .signup-wrap .box-wrap .texture {\n      position: absolute;\n      left: 0;\n      right: 0;\n      bottom: 30px;\n      text-align: center; }\n  @media (max-width: 767px) {\n        .signup-wrap .box-wrap .texture {\n          bottom: 10px; } }\n"

/***/ }),

/***/ "./src/app/account/signup/signup.component.ts":
/*!****************************************************!*\
  !*** ./src/app/account/signup/signup.component.ts ***!
  \****************************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SignupComponent = /** @class */ (function () {
    function SignupComponent(formBuilder) {
        this.formBuilder = formBuilder;
        this.showPassword = false;
    }
    SignupComponent.prototype.ngOnInit = function () {
        this.userSignupValidations = this.formBuilder.group({
            name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            passowrd: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]]
        });
    };
    SignupComponent.prototype.signup = function () {
        console.log(this.userSignupValidations);
    };
    SignupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/account/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.scss */ "./src/app/account/signup/signup.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/app/app-footer/app-footer.component.html":
/*!******************************************************!*\
  !*** ./src/app/app-footer/app-footer.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"footer\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row text-xs-left text-sm-left text-md-left\">\r\n\t\t\t<div class=\"col-12 col-sm-8 col-md-8 col-lg-3 col-xl-3\">\r\n\t\t\t\t<a href=\"/\"><img class=\"ftr-logo\" src=\"./assets/img/f-logo.png\" alt=\"Logo\" style=\"width:70px;\"></a>\r\n\t\t\t\t<p>Gumbo beet greens corn soko endive gum gourd. Parsley allot courgette tatsoi pea sprouts fava bean soluta nobis est ses eligendi option</p>\r\n\t\t\t\t<ul class=\"list-unstyled list-inline social\">\r\n\t\t\t\t\t<li class=\"list-inline-item\"><a href=\"https://www.facebook.com/mahesh.mhaske.9\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a></li>\r\n\t\t\t\t\t<li class=\"list-inline-item\"><a href=\"https://twitter.com/maheshmhaske111\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a></li>\r\n\t\t\t\t\t<li class=\"list-inline-item\"><a href=\"https://www.instagram.com/maheshmhaske1122/\" target=\"_blank\"><i class=\"fa fa-instagram\"></i></a></li>\r\n\t\t\t\t\t<li class=\"list-inline-item\"><a href=\"mailto: maheshmhaske1122.com\" target=\"_blank\"><i class=\"fa fa-envelope\"></i></a></li>\r\n\t\t\t\t\t<li class=\"list-inline-item\"><a href=\"https://www.linkedin.com/in/mahesh-mhaske-614219110\" target=\"_blank\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n\t\t\t\t</ul>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-12 col-sm-4 col-md-4 col-lg-2 col-xl-2\">\r\n\t\t\t\t<h5>Information</h5>\r\n\t\t\t\t<ul class=\"list-unstyled quick-links\">\r\n\t\t\t\t\t<li><a routerLink=\"about-us\" routerLinkActive=\"active\">About Us</a></li>\r\n\t\t\t\t\t<li><a routerLink=\"contact-us\" routerLinkActive=\"active\">Contact Us</a></li>\r\n\t\t\t\t\t<li><a href=\"\">Blog</a></li>\r\n\t\t\t\t\t<li><a href=\"\">Wishlist</a></li>\r\n\t\t\t\t</ul>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-12 col-sm-4 col-md-4 col-lg-2 col-xl-2\">\r\n\t\t\t\t<h5>Quick Links</h5>\r\n\t\t\t\t<ul class=\"list-unstyled quick-links\">\r\n\t\t\t\t\t<li><a routerLink=\"return-policy\" routerLinkActive=\"active\">Return Policy</a></li>\r\n\t\t\t\t\t<li><a routerLink=\"faq\" routerLinkActive=\"active\">Shiping Information</a></li>\r\n\t\t\t\t\t<li><a routerLink=\"terms\" routerLinkActive=\"active\">Terms & Conditions</a></li>\r\n\t\t\t\t\t<li><a href=\"\">Customer Service</a></li>\r\n\t\t\t\t</ul>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-12 col-sm-4 col-md-4 col-lg-2 col-xl-2\">\r\n\t\t\t\t<h5>Other Links</h5>\r\n\t\t\t\t<ul class=\"list-unstyled quick-links\">\r\n\t\t\t\t\t<li><a href=\"\">Skin Care</a></li>\r\n\t\t\t\t\t<li><a href=\"\">Foundations</a></li>\r\n\t\t\t\t\t<li><a href=\"\">Perfumes</a></li>\r\n\t\t\t\t\t<li><a href=\"\">Bundle Product</a></li>\r\n\t\t\t\t</ul>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-12 col-sm-4 col-md-4 col-lg-3 col-xl-3\">\r\n\t\t\t\t<h5>Contact</h5>\r\n\t\t\t\t<div class=\"media pb-2\">\r\n\t\t\t\t\t<img src=\"./assets/img/location.png\" class=\"align-self-start mr-3\" alt=\"Location\">\r\n\t\t\t\t\t<div class=\"media-body\">\r\n\t\t\t\t\t\t<p>414501 Renukaiwadi Ahmednagar  <br> Maharashtra <br> India</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"media pb-2\">\r\n\t\t\t\t\t<img src=\"./assets/img/envelope.png\" class=\"align-self-start mr-3\" alt=\"Envelope\">\r\n\t\t\t\t\t<div class=\"media-body\">\r\n\t\t\t\t\t\t<a href=\"mailto: maheshmhaske1122.com\">maheshmhaske1122.com</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"media pb-2\">\r\n\t\t\t\t\t<img src=\"./assets/img/telephone.png\" class=\"align-self-start mr-3\" alt=\"Telephone\">\r\n\t\t\t\t\t<div class=\"media-body\">\r\n\t\t\t\t\t\t<a href=\"tel: +919960627691\" >9960627691</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white\">\r\n\t\t\t\t<div class=\"copy-right\">\r\n\t\t\t\t\t<p>@ Mahi Mhaske | 2020</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\t\r\n\t</div>\r\n</section>"

/***/ }),

/***/ "./src/app/app-footer/app-footer.component.scss":
/*!******************************************************!*\
  !*** ./src/app/app-footer/app-footer.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#footer {\n  background: #000000;\n  padding: 60px 0 0; }\n  @media (max-width: 767px) {\n    #footer {\n      padding: 30px 0 0; } }\n  #footer h5 {\n    font-size: 18px;\n    color: #888888;\n    font-weight: 600;\n    font-family: \"Libre Franklin\", sans-serif;\n    margin-bottom: 26px; }\n  #footer a {\n    color: #ffffff;\n    text-decoration: none !important;\n    background-color: transparent;\n    -webkit-text-decoration-skip: objects; }\n  #footer p {\n    font-size: 14px;\n    color: #888888;\n    font-weight: 400;\n    font-family: \"Libre Franklin\", sans-serif;\n    line-height: 22px;\n    margin-bottom: 0; }\n  #footer .ftr-logo {\n    margin-bottom: 20px; }\n  #footer ul.social li {\n    padding: 8px 0;\n    background-color: #222222;\n    border-radius: 50%;\n    width: 40px;\n    height: 40px;\n    text-align: center;\n    margin-bottom: 10px; }\n  #footer ul.social li a i {\n      font-size: 20px;\n      color: #fff;\n      transition: .5s all ease; }\n  #footer ul.social li:hover, #footer ul.social li:focus {\n      background-color: #cfa092; }\n  #footer ul.social li:hover a i, #footer ul.social li:focus a i {\n      color: #fff; }\n  #footer ul.social li a {\n      font-size: 14px;\n      color: #fff;\n      font-weight: 400;\n      font-family: \"Libre Franklin\", sans-serif;\n      line-height: 22px; }\n  #footer ul.quick-links li a {\n    color: #ffffff;\n    font-weight: 400;\n    font-family: \"Libre Franklin\", sans-serif;\n    font-size: 14px; }\n  #footer ul.social {\n    margin: 20px 0; }\n  #footer ul.social li a:hover, #footer ul.focus li a:hover {\n    color: #eeeeee; }\n  #footer ul.quick-links li {\n    padding: 3px 0;\n    transition: .5s all ease; }\n  #footer .media p {\n    color: #fff;\n    padding-left: 6px; }\n  #footer .media a {\n    font-size: 14px;\n    color: #fff;\n    font-weight: 400;\n    font-family: \"Libre Franklin\", sans-serif;\n    line-height: 22px; }\n  #footer .copy-right {\n    border-top: 1px solid #222222; }\n  #footer .copy-right p {\n      margin: 0;\n      padding: 30px 0;\n      font-weight: 400;\n      font-size: 14px;\n      color: #ababab;\n      font-family: \"Rubik\", sans-serif; }\n"

/***/ }),

/***/ "./src/app/app-footer/app-footer.component.ts":
/*!****************************************************!*\
  !*** ./src/app/app-footer/app-footer.component.ts ***!
  \****************************************************/
/*! exports provided: AppFooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppFooterComponent", function() { return AppFooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppFooterComponent = /** @class */ (function () {
    function AppFooterComponent() {
    }
    AppFooterComponent.prototype.ngOnInit = function () {
    };
    AppFooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-app-footer',
            template: __webpack_require__(/*! ./app-footer.component.html */ "./src/app/app-footer/app-footer.component.html"),
            styles: [__webpack_require__(/*! ./app-footer.component.scss */ "./src/app/app-footer/app-footer.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AppFooterComponent);
    return AppFooterComponent;
}());



/***/ }),

/***/ "./src/app/app-header/app-header.component.html":
/*!******************************************************!*\
  !*** ./src/app/app-header/app-header.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"top-strip\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-2 col-sm-5 col-md-5 col-lg-5 col-xg-5\"><a href=\"tel: +919960627691\" class=\"number d-none d-sm-block\"><span>Customer Support</span> (9960627691)</a>\r\n        <a href=\"tel: +919960627691\" class=\"d-block d-sm-none\"><img src=\"./assets/img/phone.png\" alt=\"Call\"></a></div>\r\n      <div class=\"col-10 col-sm-7 col-md-7 col-lg-7 col-xg-7\">\r\n        <ul class=\"nav justify-content-end\">\r\n          <li class=\"nav-item\"><a routerLink=\"login\" routerLinkActive=\"active\">Login</a><span>or</span><a routerLink=\"signup\" routerLinkActive=\"active\">Sign Up</a></li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<nav class=\"my-header navbar navbar-expand-md fixed-top\">\r\n  <div class=\"container\">\r\n  <a class=\"navbar-brand\"  routerLink=\"/\" routerLinkActive=\"active\"><img src=\"./assets/img/logo.png\" alt=\"Logo\" style=\"width:70px;\"></a>\r\n  <div class=\"d-block d-sm-none\">\r\n    <ul  class=\"nav justify-content-end\">\r\n      <li class=\"nav-item\">\r\n          <a class=\"nav-link\" (click)=\"showSearch($event)\"><img src=\"./assets/img/search-xs.png\" alt=\"Search\"></a>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" routerLink=\"/\"><img src=\"./assets/img/like-xs.png\" alt=\"Like\"></a>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" routerLink=\"cart\" routerLinkActive=\"active\"><img src=\"./assets/img/shopping-bag-xs.png\" alt=\"Shopping Bag\"></a>\r\n        </li>\r\n    </ul>\r\n  </div>\r\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">\r\n   <img src=\"./assets/img/menu.png\" class=\"\" alt=\"Hamburger\">\r\n  </button>\r\n  <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">\r\n    <ul class=\"navbar-nav ml-auto\">\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/\" routerLinkActive=\"active\">Home</a>\r\n      </li>\r\n      <li class=\"nav-item\" >\r\n        <a class=\"nav-link\" routerLink=\"products\" routerLinkActive=\"active\">Shop</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"faq\" routerLinkActive=\"active\">How it Works</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"signup\" routerLinkActive=\"active\">Join</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"about-us\" routerLinkActive=\"active\">Our Story</a>\r\n      </li>\r\n      <li class=\"nav-item right-nav d-none d-sm-block\">\r\n        <a class=\"nav-link pad-15\"(click)=\"showSearch($event)\" ><img src=\"./assets/img/search.png\" alt=\"Search\"></a>\r\n      </li>\r\n      <li class=\"nav-item d-none d-sm-block\">\r\n        <a class=\"nav-link pad-15\" routerLink=\"wishlist\" routerLinkActive=\"active\"><img src=\"./assets/img/like.png\" alt=\"Like\"></a>\r\n      </li>\r\n      <li class=\"nav-item d-none d-sm-block\">\r\n        <a class=\"nav-link pad-15\" routerLink=\"cart\" routerLinkActive=\"active\"><img src=\"./assets/img/shopping-bag.png\" alt=\"Shopping Bag\"> <span class=\"bag-price\">0 / $0.00</span></a>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n  </div>\r\n</nav>\r\n<mat-progress-bar\r\n  *ngIf=\"loading\" color=\"accent\" mode=\"indeterminate\">\r\n</mat-progress-bar>\r\n\r\n"

/***/ }),

/***/ "./src/app/app-header/app-header.component.scss":
/*!******************************************************!*\
  !*** ./src/app/app-header/app-header.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-progress-bar {\n  z-index: 9999; }\n\n.top-strip {\n  position: fixed;\n  top: 0;\n  right: 0;\n  left: 0;\n  z-index: 9;\n  background-color: #000;\n  padding: 8px 0; }\n\n.top-strip .nav .nav-item a {\n    padding-right: 0;\n    padding-left: 0;\n    font-weight: 500;\n    font-family: \"Libre Franklin\", sans-serif;\n    color: #cfa092;\n    font-size: 14px; }\n\n@media (max-width: 480px) {\n      .top-strip .nav .nav-item a {\n        padding-right: 0;\n        padding-left: 0;\n        font-size: 12px; } }\n\n.top-strip .nav .nav-item span {\n    color: #888888;\n    font-size: 14px;\n    padding: 0 4px; }\n\n.top-strip .nav .nav-item:last-child a {\n    padding-right: 0; }\n\n.top-strip .number {\n    font-weight: 600;\n    color: #ffffff;\n    font-size: 12px;\n    line-height: 24px; }\n\n.top-strip .number span {\n      font-weight: 400; }\n\n.my-header {\n  background-color: #fff;\n  font-family: \"Libre Baskerville\", serif;\n  font-size: 14px;\n  font-weight: 700;\n  box-shadow: 0 3px 5px -1px rgba(0, 0, 0, 0.2), 0 6px 10px 0 rgba(0, 0, 0, 0.14), 0 1px 18px 0 rgba(0, 0, 0, 0.12); }\n\n.my-header.fixed-top {\n    top: 40px;\n    padding: 8px 0;\n    z-index: 9; }\n\n@media (max-width: 767px) {\n      .my-header.fixed-top {\n        padding: 4px 15px; } }\n\n.my-header.fixed-top .nav {\n      padding-left: 40px; }\n\n.my-header.fixed-top .nav .nav-link {\n        padding: 8px 10px; }\n\n.my-header .navbar-nav a {\n    color: #000000;\n    padding: 8px 20px; }\n\n@media (min-width: 768px) and (max-width: 991px) {\n      .my-header .navbar-nav a {\n        padding: 8px 10px; } }\n\n@media (max-width: 767px) {\n      .my-header .navbar-nav a {\n        padding: 10px 10px; } }\n\n.my-header .navbar-nav .is-active {\n    color: #cfa092; }\n\n.my-header .navbar-nav .nav-item:last-child a {\n    padding-right: 0; }\n\n.my-header .navbar-nav .right-nav {\n    margin-left: 40px;\n    /* Portrait tablets and small desktops */ }\n\n@media (min-width: 768px) and (max-width: 991px) {\n      .my-header .navbar-nav .right-nav {\n        margin-left: 0; } }\n\n@media (max-width: 767px) {\n      .my-header .navbar-nav .right-nav {\n        margin-left: 0; } }\n\n.my-header .navbar-nav .pad-15 {\n    padding: 8px 10px; }\n\n@media (max-width: 767px) {\n      .my-header .navbar-nav .pad-15 {\n        padding: 8px 10px; } }\n\n.my-header .navbar-nav .bag-price {\n    font-size: 12px;\n    color: #000000;\n    font-weight: 400;\n    padding-left: 6px;\n    font-family: \"Libre Franklin\", sans-serif; }\n\n@media (max-width: 767px) {\n    .my-header .navbar-collapse {\n      border-top: 2px solid #767676;\n      margin-top: 8px;\n      height: 70vh;\n      overflow-x: scroll; } }\n\n@media (max-width: 480px) {\n    .my-header .navbar-collapse {\n      height: 86vh;\n      overflow-x: scroll; } }\n\n@media (max-width: 767px) {\n    .my-header .navbar-collapse .nav-item {\n      border-bottom: 1px solid #e3e3e3; } }\n\n@media (max-width: 767px) {\n    .my-header .navbar-collapse .nav-item:last-child {\n      border-bottom: 1px solid transparent; } }\n\n.my-header .navbar-toggler {\n    padding: 0; }\n\n.my-header .navbar-toggler:focus {\n      text-decoration: none;\n      outline: none; }\n"

/***/ }),

/***/ "./src/app/app-header/app-header.component.ts":
/*!****************************************************!*\
  !*** ./src/app/app-header/app-header.component.ts ***!
  \****************************************************/
/*! exports provided: AppHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppHeaderComponent", function() { return AppHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppHeaderComponent = /** @class */ (function () {
    function AppHeaderComponent(router) {
        var _this = this;
        this.router = router;
        this.loading = false;
        var cloasEle = document.getElementsByClassName('navbar-collapse');
        this.router.events.subscribe(function (event) {
            switch (true) {
                case event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationStart"]: {
                    _this.loading = true;
                    break;
                }
                case event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]:
                case event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationCancel"]:
                case event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationError"]: {
                    _this.loading = false;
                    if (cloasEle[0].classList.contains("show")) {
                        cloasEle[0].classList.remove("show");
                    }
                    break;
                }
                default: {
                    break;
                }
            }
        });
    }
    AppHeaderComponent.prototype.ngOnInit = function () {
    };
    AppHeaderComponent.prototype.showSearch = function ($event) {
        var searchEle = document.getElementsByClassName('search-main');
        if (searchEle[0].classList.contains("search--open")) {
            searchEle[0].classList.remove("search--open");
        }
        else {
            searchEle[0].classList.add("search--open");
        }
    };
    AppHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-app-header',
            template: __webpack_require__(/*! ./app-header.component.html */ "./src/app/app-header/app-header.component.html"),
            styles: [__webpack_require__(/*! ./app-header.component.scss */ "./src/app/app-header/app-header.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AppHeaderComponent);
    return AppHeaderComponent;
}());



/***/ }),

/***/ "./src/app/app-home/app-home.component.html":
/*!**************************************************!*\
  !*** ./src/app/app-home/app-home.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"top-space\">\r\n  <div class=\"banner-main\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-9 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\r\n          <div class=\"banner-wrap\">\r\n            <div class=\"banner-wrap-info\">\r\n              <h1>Check out our new make-up swatches of 2020</h1>\r\n              <h4 class=\"d-none d-sm-block\">Extra peedling, and effective and excellent formulation of fresh fruit-natural pear and vitamin C</h4>\r\n              <button type=\"button\" class=\"btn btn-pri\" routerLink=\"/products\" >Shop Now</button>\r\n            </div>\r\n            \r\n          </div>\r\n        </div>\r\n      </div> \r\n    </div>   \r\n  </div>\r\n</section>\r\n<section class=\"ptb-40\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4\">\r\n        <div class=\"trust-wrap\">\r\n          <div class=\"media\">\r\n            <img src=\"./assets/img/fd.png\" class=\" mr-3\" alt=\"Telephone\">\r\n            <div class=\"media-body align-self-center\">\r\n              <h1>Free Delivery</h1>\r\n              <p>For order over $150</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4\">\r\n        <div class=\"trust-wrap\">\r\n          <div class=\"media\">\r\n            <img src=\"./assets/img/fd.png\" class=\" mr-3\" alt=\"Telephone\">\r\n            <div class=\"media-body align-self-center\">\r\n              <h1>30 Days Return</h1>\r\n              <p>30 Day Money Back Guarantee</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4\">\r\n        <div class=\"trust-wrap\">\r\n          <div class=\"media\">\r\n            <img src=\"./assets/img/fd.png\" class=\"mr-3\" alt=\"Telephone\">\r\n            <div class=\"media-body align-self-center\">\r\n              <h1>Secure Payment</h1>\r\n              <p>100% safe payment</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section class=\"ptb-40\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <h2 class=\"text-center section-head\">Featured Categories</h2>\r\n        <div class=\"text-center\">\r\n          <img alt=\"Title\" src=\"./assets/img/title-icon.png\" class=\"\">\r\n        </div>\r\n        <p class=\"text-center section-sub-head\">Feature category to display category on any pages demo</p>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6 col-sm-12 co-xs-12 gal-item\">\r\n        <div class=\"row h-40\">\r\n          <div class=\"col-md-12 col-sm-12 co-xs-12 gal-item\">\r\n            <div class=\"box\">\r\n              <img src=\"./assets/img/col-1.png\" class=\"img-ht img-fluid\">\r\n              <div class=\"info-box card card-block w-75 mx-auto text-center\">\r\n                <h3>Eye Liners</h3>\r\n                <div class=\"text-center\">\r\n                  <button type=\"button\" class=\"btn btn-pri\" routerLink=\"/products\">Shop Now</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row h-60\">\r\n          <div class=\"col-md-12 col-sm-12 co-xs-12 gal-item\">\r\n            <div class=\"box\">\r\n              <img src=\"./assets/img/col-3.png\" class=\"img-ht img-fluid\">\r\n              <div class=\"info-box card card-block w-75 mx-auto text-center\">\r\n                <h3>Lipsticks</h3>\r\n                <div class=\"text-center\">\r\n                  <button type=\"button\" class=\"btn btn-pri\" routerLink=\"/products\">Shop Now</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-6 col-sm-12 co-xs-12 gal-item\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-12 col-sm-12 co-xs-12 gal-item h-60\">\r\n            <div class=\"box\">\r\n              <img src=\"./assets/img/col-2.png\" class=\"img-ht img-fluid\">\r\n              <div class=\"info-box card card-block w-75 mx-auto text-center\">\r\n                <h3>Brushes</h3>\r\n                <div class=\"text-center\">\r\n                  <button type=\"button\" class=\"btn btn-pri\" routerLink=\"/products\">Shop Now</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-12 col-sm-12 co-xs-12 gal-item h-40\">\r\n            <div class=\"box\">\r\n              <img src=\"./assets/img/col-4.png\" class=\"img-ht img-fluid\">\r\n              <div class=\"info-box card card-block w-75 mx-auto text-center\">\r\n                <h3>Make-up Set</h3>\r\n                <div class=\"text-center\">\r\n                  <button type=\"button\" class=\"btn btn-pri\" routerLink=\"/products\">Shop Now</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section class=\"new-arrivals ptb-40\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <h2 class=\"text-center section-head\">New Arrivals</h2>\r\n        <div class=\"text-center\">\r\n          <img alt=\"Title\" src=\"./assets/img/title-icon.png\" class=\"\">\r\n        </div>\r\n        <p class=\"text-center section-sub-head\">Add our new arrivals to your weekly lineup</p>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <mat-tab-group mat-align-tabs=\"center\" animationDuration=\"0ms\">\r\n          <mat-tab label=\"All\">\r\n            <app-item-swiper [swiperMessage]=\"config\"></app-item-swiper>\r\n          </mat-tab>\r\n          <mat-tab label=\"Foundation\">\r\n            <app-item-swiper [swiperMessage]=\"config\"></app-item-swiper>\r\n          </mat-tab>\r\n          <mat-tab label=\"Lipsticks\">\r\n            <app-item-swiper [swiperMessage]=\"config\"></app-item-swiper>\r\n          </mat-tab>\r\n          <mat-tab label=\"Skin Care\">\r\n            <app-item-swiper [swiperMessage]=\"config\"></app-item-swiper>\r\n          </mat-tab>\r\n          <mat-tab label=\"Sale\">\r\n            <app-item-swiper [swiperMessage]=\"config\"></app-item-swiper>\r\n          </mat-tab>\r\n        </mat-tab-group>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section class=\"offer-section ptb-40\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-8 col-md-8 col-lg-6 col-xl-6\">\r\n        <div class=\"offer-wrap\">\r\n          <p class=\"offer-head text-center\">Like and Share Our Page for One Time</p>\r\n          <h3 class=\"off text-center\">10% <sup>Off</sup></h3>\r\n            <div class=\"text-center social-icon\">\r\n              <a href=\"\"><i class=\"fa fa-facebook\"></i></a>\r\n              <a href=\"\"><i class=\"fa fa-twitter\"></i></a>\r\n              <a href=\"\"><i class=\"fa fa-instagram\"></i></a>\r\n              <a href=\"\" target=\"_blank\"><i class=\"fa fa-envelope\"></i></a>\r\n              <a href=\"\"><i class=\"fa fa-google-plus\"></i></a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section class=\"best-sellers ptb-40\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <h2 class=\"text-center section-head\">Best Sellers</h2>\r\n        <div class=\"text-center\">\r\n          <img alt=\"Title\" src=\"./assets/img/title-icon.png\" class=\"\">\r\n        </div>\r\n        <p class=\"text-center section-sub-head\">Add our best sellers to your weekly lineup</p>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <mat-tab-group mat-align-tabs=\"center\" animationDuration=\"0ms\">\r\n          <mat-tab label=\"All\">\r\n            <app-item-swiper [swiperMessage]=\"config\"></app-item-swiper>\r\n          </mat-tab>\r\n          <mat-tab label=\"Foundation\">\r\n            <app-item-swiper [swiperMessage]=\"config\"></app-item-swiper>\r\n          </mat-tab>\r\n          <mat-tab label=\"Lipsticks\">\r\n            <app-item-swiper [swiperMessage]=\"config\"></app-item-swiper>\r\n          </mat-tab>\r\n          <mat-tab label=\"Skin Care\">\r\n            <app-item-swiper [swiperMessage]=\"config\"></app-item-swiper>\r\n          </mat-tab>\r\n          <mat-tab label=\"Sale\">\r\n            <app-item-swiper [swiperMessage]=\"config\"></app-item-swiper>\r\n          </mat-tab>\r\n        </mat-tab-group>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section class=\"latest-blog-posts ptb-40\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <h2 class=\"text-center section-head\">Latest Blog Posts</h2>\r\n        <div class=\"text-center\">\r\n          <img alt=\"Title\" src=\"./assets/img/title-icon.png\" class=\"\">\r\n        </div>\r\n        <p class=\"text-center section-sub-head\">Check out our latest blog posts</p>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6\">\r\n        <div class=\"blog-wrap\">\r\n          <div class=\"blog-img-wrap\">\r\n            <img src=\"./assets/img/b1.png\" alt=\"Blog\" class=\"img-fluid\">\r\n          </div>\r\n          <div class=\"blog-info w-75 text-center\">\r\n            <h2>10 quick tips for makeup</h2>\r\n            <p>By admin / January 13, 2018</p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6\">\r\n        <div class=\"blog-wrap\">\r\n          <div class=\"blog-img-wrap\">\r\n            <img src=\"./assets/img/b2.png\" alt=\"Blog\" class=\"img-fluid\">\r\n          </div>\r\n          <div class=\"blog-info w-75 text-center\">\r\n            <h2>10 quick tips for makeup</h2>\r\n            <p>By admin / January 13, 2018</p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section class=\"newsletter-section ptb-20\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <h2 class=\"text-center section-head\">Subscribe to Our Newsletter</h2>\r\n      </div>\r\n      <div class=\"col-12 offset-sm-2 col-sm-8 offset-md-2 col-md-8 offset-lg-2 col-lg-8 offset-xl-2 col-xg-8\">\r\n        <div class=\"input-group\">\r\n          <input type=\"email\" class=\"form-control\" placeholder=\"Enter email address\" id=\"email\">\r\n          <div class=\"input-group-append\">\r\n            <button type=\"submit\" class=\"btn btn-subscribe\">Subscribe</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <p class=\"text-center nl-text\">Sign up for Exclusive Updates, New Arrivals and Insider-Only Discount.</p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section  class=\"customer-say\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <div class=\"testi-wrap\">\r\n          <h2 class=\"text-center section-head\">Customer Say </h2>\r\n          <div class=\"text-center\">\r\n            <img alt=\"Title\" src=\"./assets/img/title-icon.png\" class=\"\">\r\n          </div>\r\n          <swiper class=\"swiper-container\" [config]=\"customerConfig\" [(index)]=\"index\">\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"say-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n                  <h3>Michael</h3>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"say-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n                  <h3>Michael</h3>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"say-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n                  <h3>Michael</h3>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </swiper>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section  class=\"brand ptb-40\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <div class=\"brand-wrap\">\r\n          <swiper class=\"swiper-container\" [config]=\"brandcustomerConfig\" [(index)]=\"index\">\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"brand-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <img alt=\"Title\" src=\"./assets/img/br1.png\" class=\"img-fluid\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"brand-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <img alt=\"Title\" src=\"./assets/img/br2.png\" class=\"img-fluid\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"brand-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <img alt=\"Title\" src=\"./assets/img/br3.png\" class=\"img-fluid\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"brand-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <img alt=\"Title\" src=\"./assets/img/br4.png\" class=\"img-fluid\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"brand-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <img alt=\"Title\" src=\"./assets/img/br5.png\" class=\"img-fluid\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </swiper>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section  class=\"follow-instagram ptb-40\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <h2 class=\"text-center section-head\">Follow Us on Instagram</h2>\r\n        <div class=\"text-center\">\r\n          <img alt=\"Title\" src=\"./assets/img/title-icon.png\" class=\"\">\r\n        </div>\r\n        <p class=\"text-center section-sub-head\">Folow us @reign_beauty</p>\r\n      </div>\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <div class=\"insta-wrap\">\r\n          <swiper class=\"swiper-container\" [config]=\"instaConfig\" [(index)]=\"index\">\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"insta-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <a href=\"\">\r\n                    <img alt=\"Title\" src=\"./assets/img/in1.png\" class=\"img-fluid\">\r\n                  </a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"insta-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <a href=\"\">\r\n                    <img alt=\"Title\" src=\"./assets/img/in2.png\" class=\"img-fluid\">\r\n                  </a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"insta-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <a href=\"\">\r\n                    <img alt=\"Title\" src=\"./assets/img/in3.png\" class=\"img-fluid\">\r\n                  </a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"insta-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <a href=\"\">\r\n                    <img alt=\"Title\" src=\"./assets/img/in4.png\" class=\"img-fluid\">\r\n                  </a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"insta-box\">\r\n                <div class=\"info-box text-center\">\r\n                  <a href=\"\">\r\n                    <img alt=\"Title\" src=\"./assets/img/in5.png\" class=\"img-fluid\">\r\n                  </a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </swiper>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/app-home/app-home.component.scss":
/*!**************************************************!*\
  !*** ./src/app/app-home/app-home.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".banner-main {\n  background-image: url('banner-bg.png');\n  background-repeat: no-repeat;\n  background-position: bottom;\n  background-size: cover;\n  padding: 190px 0;\n  position: relative; }\n  @media (max-width: 767px) {\n    .banner-main {\n      padding: 62px 0; } }\n  .banner-main .banner-wrap h1 {\n    font-size: 36px;\n    font-weight: 400;\n    font-family: \"Libre Baskerville\", serif;\n    color: #cfa092;\n    line-height: 44px; }\n  @media (min-width: 992px) and (max-width: 1199px) {\n      .banner-main .banner-wrap h1 {\n        font-size: 32px; } }\n  @media (max-width: 767px) {\n      .banner-main .banner-wrap h1 {\n        font-size: 16px;\n        color: #fff;\n        line-height: 30px;\n        margin-bottom: 30px; } }\n  .banner-main .banner-wrap h4 {\n    font-size: 14px;\n    font-weight: 500;\n    font-family: \"Libre Franklin\", sans-serif;\n    color: #888888;\n    line-height: 24px;\n    padding-right: 18%;\n    margin-bottom: 24px; }\n  .banner-main .banner-wrap .btn-pri {\n    background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n    font-size: 14px;\n    color: #000;\n    font-weight: 700;\n    font-family: \"Libre Baskerville\", serif;\n    padding: 18px 44px;\n    border-radius: 0;\n    border-top: 2px solid transparent; }\n  @media (max-width: 767px) {\n      .banner-main .banner-wrap .btn-pri {\n        padding: 12px 20px; } }\n  .banner-main .banner-wrap .btn-pri:hover {\n    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.6);\n    border-top: 2px solid #c6755d; }\n  .ptb-40 {\n  padding: 40px 0; }\n  @media (max-width: 767px) {\n    .ptb-40 {\n      padding: 24px 0; } }\n  .ptb-20 {\n  padding: 20px 0; }\n  .section-head {\n  font-weight: 400;\n  font-family: \"Libre Baskerville\", serif;\n  color: #000;\n  font-size: 36px;\n  margin-bottom: 16px; }\n  @media (max-width: 767px) {\n    .section-head {\n      font-size: 24px; } }\n  .section-sub-head {\n  font-weight: 400;\n  font-family: \"Libre Franklin\", sans-serif;\n  color: #666;\n  font-size: 18px;\n  margin: 18px 0 50px; }\n  @media (max-width: 767px) {\n    .section-sub-head {\n      margin: 18px 0 36px;\n      font-size: 12px; } }\n  .trust-wrap {\n  padding: 0 40px; }\n  @media (min-width: 992px) and (max-width: 1199px) {\n    .trust-wrap {\n      padding: 0; } }\n  @media (min-width: 768px) and (max-width: 991px) {\n    .trust-wrap {\n      padding: 0; } }\n  @media (max-width: 767px) {\n    .trust-wrap {\n      padding: 0;\n      margin-bottom: 18px; } }\n  .trust-wrap h1 {\n    font-weight: 400;\n    font-family: \"Libre Baskerville\", serif;\n    color: #000;\n    font-size: 20px;\n    margin-bottom: 10px; }\n  @media (min-width: 768px) and (max-width: 991px) {\n      .trust-wrap h1 {\n        font-size: 18px; } }\n  .trust-wrap p {\n    font-size: 14px;\n    font-weight: 400;\n    font-family: \"Libre Franklin\", sans-serif;\n    color: #666; }\n  .gal-item {\n  overflow: hidden;\n  margin-bottom: 20px; }\n  .gal-item .box {\n    position: relative; }\n  .gal-item .box img {\n      -o-object-fit: cover;\n         object-fit: cover; }\n  .gal-item .box .info-box {\n      position: absolute;\n      top: 30%;\n      left: 0;\n      right: 0;\n      border-width: 1px;\n      border-color: #c7c7c7;\n      border-style: solid;\n      background-color: rgba(0, 0, 0, 0.502);\n      padding: 20px 50px;\n      border-radius: 0; }\n  @media (min-width: 768px) and (max-width: 991px) {\n        .gal-item .box .info-box {\n          top: 24%; } }\n  @media (max-width: 767px) {\n        .gal-item .box .info-box {\n          top: 20%;\n          padding: 15px 40px; } }\n  .gal-item .box .info-box h3 {\n        font-weight: 400;\n        font-family: \"Libre Baskerville\", serif;\n        color: #fff;\n        font-size: 36px;\n        margin-bottom: 14px; }\n  @media (min-width: 768px) and (max-width: 991px) {\n          .gal-item .box .info-box h3 {\n            font-size: 20px; } }\n  @media (max-width: 767px) {\n          .gal-item .box .info-box h3 {\n            font-size: 20px; } }\n  .gal-item .box .info-box .btn-pri {\n        background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n        box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n        font-size: 14px;\n        color: #000;\n        font-weight: 700;\n        padding: 16px 40px;\n        font-family: \"Libre Baskerville\", serif;\n        border-radius: 0;\n        border-top: 2px solid transparent; }\n  @media (min-width: 768px) and (max-width: 991px) {\n          .gal-item .box .info-box .btn-pri {\n            padding: 12px 20px; } }\n  @media (max-width: 767px) {\n          .gal-item .box .info-box .btn-pri {\n            padding: 10px 18px; } }\n  .gal-item .box .info-box .btn-pri:hover {\n        box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.6);\n        border-top: 2px solid #c6755d;\n        color: #000 !important; }\n  .mat-tab-label {\n  font-size: 14px;\n  font-weight: 700;\n  font-family: \"Libre Franklin\", sans-serif;\n  color: #666; }\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: #cfa092; }\n  .mat-tab-label-active {\n  color: #cfa092; }\n  .mat-tab-body-wrapper {\n  margin-top: 26px; }\n  .mat-tab-header {\n  border-bottom: 1px solid transparent; }\n  /*.item-box{\r\n    position: relative;\r\n    &:hover .img-box{\r\n        border: 1px solid #ddd;\r\n    }\r\n    &:hover .img-box .action{\r\n        display: block;\r\n    }\r\n    .img-box{\r\n        position: relative;\r\n        min-height: 350px;\r\n        .action{\r\n            display: none;\r\n            position: absolute;\r\n            .action-item{\r\n                border-width: 1px;\r\n                border-color: rgb(221, 221, 221);\r\n                border-style: solid;\r\n                background-color: rgb(255, 255, 255);\r\n                padding: 12px;\r\n            } \r\n            .btn-pri{\r\n            background-image: -moz-linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\r\n            background-image: -webkit-linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\r\n            background-image: -ms-linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\r\n            background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\r\n            box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\r\n            font-size: 14px;\r\n            color: #000;\r\n            font-weight: 700;\r\n            padding: 16px 40px;\r\n            font-family: $secondaryFont;\r\n            border-radius: 0;\r\n            border-top: 2px solid transparent;\r\n            @media (min-width: 768px) and (max-width: 991px) {\r\n              padding: 12px 20px;\r\n            }\r\n            @media (max-width: 767px) {\r\n                padding: 16px 22px;\r\n            }\r\n        }\r\n        .btn-pri:hover{\r\n            box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.6);\r\n            border-top: 2px solid #c6755d;\r\n            color: #000 !important;\r\n        }\r\n        }\r\n        .tl{\r\n            right: 0;\r\n            top: 0;\r\n        }\r\n        .bc{\r\n            bottom: 20px;\r\n            left: 0;\r\n            right: 0;\r\n            text-align: center;\r\n        }\r\n    }\r\n    .info-box{\r\n        h1{\r\n            font-size: 14px;\r\n            font-weight: 400;\r\n            font-family: $primaryFont;\r\n            color: #000;\r\n            margin: 15px 0;\r\n            &:hover{\r\n                color: #cfa092;\r\n            }\r\n        }\r\n        p{\r\n            font-size: 16px;\r\n            font-weight: 600;\r\n            font-family: $primaryFont;\r\n            color: #000;\r\n            del{\r\n                font-size: 16px;\r\n                font-weight: 400;\r\n                font-family: $primaryFont;\r\n                color: #000;\r\n                padding-right: 10px;\r\n            }\r\n        }\r\n    }\r\n}\r\n*/\n  .offer-section {\n  background-image: url('off-bg.png');\n  background-repeat: no-repeat;\n  background-position: bottom;\n  background-size: cover;\n  padding: 100px 0;\n  position: relative;\n  margin-bottom: 40px; }\n  .offer-section .offer-wrap {\n    border-width: 1px;\n    border-color: #888888;\n    border-style: solid;\n    padding: 30px 40px;\n    position: relative; }\n  .offer-section .offer-wrap .offer-head {\n      color: #fff;\n      font-weight: 400;\n      padding: 0;\n      margin-bottom: 20px;\n      font-size: 20px;\n      font-family: \"Libre Baskerville\", serif; }\n  .offer-section .offer-wrap .off {\n      color: #cfa092;\n      font-weight: 400;\n      font-size: 80px;\n      font-family: \"Libre Baskerville\", serif; }\n  .offer-section .offer-wrap .off sup {\n        font-size: 30px;\n        top: -2.5rem; }\n  .offer-section .offer-wrap .social-icon {\n      position: absolute;\n      bottom: -12px;\n      background-color: #000;\n      left: 0;\n      right: 0;\n      width: 60%;\n      margin: auto; }\n  .offer-section .offer-wrap .social-icon a {\n        color: #fff;\n        padding: 0 10px;\n        font-size: 20px; }\n  .latest-blog-posts {\n  background-color: #000;\n  padding-bottom: 150px; }\n  @media (max-width: 767px) {\n    .latest-blog-posts {\n      padding-bottom: 0; } }\n  .latest-blog-posts .section-head {\n    color: #fff; }\n  .latest-blog-posts .section-sub-head {\n    color: #888; }\n  .latest-blog-posts .blog-wrap {\n    position: relative; }\n  @media (max-width: 767px) {\n      .latest-blog-posts .blog-wrap {\n        margin-bottom: 6rem; } }\n  .latest-blog-posts .blog-wrap .blog-info {\n      position: absolute;\n      background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n      box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n      bottom: -3rem;\n      padding: 26px 0;\n      left: 0;\n      right: 0;\n      margin: auto; }\n  @media (min-width: 768px) and (max-width: 991px) {\n        .latest-blog-posts .blog-wrap .blog-info {\n          padding: 20px 0; } }\n  @media (max-width: 767px) {\n        .latest-blog-posts .blog-wrap .blog-info {\n          padding: 18px 0; } }\n  .latest-blog-posts .blog-wrap .blog-info h2 {\n        color: #010101;\n        font-weight: 400;\n        font-size: 20px;\n        font-family: \"Libre Baskerville\", serif;\n        margin-bottom: 14px; }\n  @media (min-width: 768px) and (max-width: 991px) {\n          .latest-blog-posts .blog-wrap .blog-info h2 {\n            font-size: 16px; } }\n  @media (max-width: 767px) {\n          .latest-blog-posts .blog-wrap .blog-info h2 {\n            font-size: 14px; } }\n  .latest-blog-posts .blog-wrap .blog-info p {\n        color: #666666;\n        font-size: 14px;\n        font-weight: 400;\n        font-family: \"Libre Franklin\", sans-serif;\n        margin: 0; }\n  .newsletter-section {\n  background-image: url('nl-bg.png');\n  background-repeat: no-repeat;\n  background-position: top;\n  background-size: cover;\n  padding: 50px 0; }\n  .newsletter-section .nl-text {\n    text-shadow: 2px 4px 3px rgba(230, 230, 230, 0.3);\n    font-weight: 400;\n    font-family: \"Libre Franklin\", sans-serif;\n    color: #000;\n    font-size: 14px;\n    margin-top: 22px;\n    padding-bottom: 70px; }\n  @media (max-width: 767px) {\n      .newsletter-section .nl-text {\n        padding-bottom: 20px; } }\n  .newsletter-section .section-head {\n    margin-bottom: 48px; }\n  .newsletter-section input {\n    height: 50px;\n    border-radius: 0;\n    border: 0; }\n  .newsletter-section .input-group-append {\n    margin-left: 10px; }\n  @media (max-width: 767px) {\n      .newsletter-section .input-group-append {\n        margin-left: 0;\n        margin-top: 12px;\n        display: block;\n        width: 100%;\n        text-align: center; } }\n  .newsletter-section .btn-subscribe {\n    background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n    font-weight: 700;\n    font-size: 14px;\n    color: #000;\n    height: 50px;\n    padding: 0px 30px;\n    font-family: \"Libre Baskerville\", serif; }\n  .newsletter-section .btn-subscribe:hover {\n      box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.4); }\n  .customer-say {\n  margin-top: -48px; }\n  .customer-say .testi-wrap {\n    background-color: #000000;\n    padding: 28px 40px; }\n  @media (max-width: 767px) {\n      .customer-say .testi-wrap {\n        padding: 22px 20px; } }\n  .customer-say .testi-wrap .section-head {\n      color: #fff; }\n  .customer-say .testi-wrap p {\n      font-weight: 400;\n      font-family: \"Libre Franklin\", sans-serif;\n      color: #fff;\n      font-size: 14px;\n      margin-bottom: 15px; }\n  .customer-say .testi-wrap h3 {\n      font-weight: 500;\n      font-family: \"Libre Baskerville\", serif;\n      color: #cfa092;\n      font-size: 14px;\n      margin: 0 0 20px; }\n  .customer-say .testi-wrap .swiper-pagination-bullet {\n      border-radius: 0;\n      background: #888;\n      opacity: 1; }\n  .customer-say .testi-wrap .swiper-pagination-bullet-active {\n      background: #fff; }\n  .customer-say .testi-wrap .say-box {\n      padding: 15px 24px 24px; }\n  @media (max-width: 767px) {\n        .customer-say .testi-wrap .say-box {\n          padding: 14px 2px 14px; } }\n  .follow-instagram .section-sub-head {\n  margin: 18px 0 30px; }\n"

/***/ }),

/***/ "./src/app/app-home/app-home.component.ts":
/*!************************************************!*\
  !*** ./src/app/app-home/app-home.component.ts ***!
  \************************************************/
/*! exports provided: AppHomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppHomeComponent", function() { return AppHomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppHomeComponent = /** @class */ (function () {
    function AppHomeComponent(elementRef) {
        this.elementRef = elementRef;
        this.config = {
            direction: 'horizontal',
            slidesPerView: 4,
            keyboard: true,
            mousewheel: false,
            scrollbar: false,
            autoHeight: false,
            setWrapperSize: false,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            navigation: true,
            spaceBetween: 30,
            pagination: false,
            breakpoints: {
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                480: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                580: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 30
                }
            },
            observer: true,
            observeParents: true
        };
        this.customerConfig = {
            direction: 'horizontal',
            slidesPerView: 1,
            keyboard: true,
            mousewheel: false,
            scrollbar: false,
            autoHeight: false,
            setWrapperSize: false,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
            },
            navigation: false,
            spaceBetween: 10,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            observer: true,
            observeParents: true
        };
        this.brandcustomerConfig = {
            direction: 'horizontal',
            slidesPerView: 5,
            keyboard: true,
            centeredSlides: true,
            loop: true,
            mousewheel: false,
            scrollbar: false,
            autoHeight: false,
            setWrapperSize: false,
            speed: 3000,
            navigation: false,
            spaceBetween: 30,
            pagination: false,
            breakpoints: {
                320: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                480: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                580: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 20
                }
            },
            observer: true,
            observeParents: true
        };
        this.instaConfig = {
            direction: 'horizontal',
            slidesPerView: 5,
            keyboard: true,
            loop: true,
            mousewheel: false,
            scrollbar: false,
            autoHeight: false,
            setWrapperSize: false,
            speed: 5000,
            autoplay: {
                delay: 0,
                disableOnInteraction: false,
            },
            navigation: false,
            spaceBetween: 0,
            pagination: false,
            breakpoints: {
                320: {
                    slidesPerView: 2
                },
                480: {
                    slidesPerView: 2
                },
                580: {
                    slidesPerView: 3
                },
                768: {
                    slidesPerView: 4
                }
            },
            observer: true,
            observeParents: true
        };
    }
    AppHomeComponent.prototype.ngOnInit = function () {
    };
    AppHomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-app-home',
            template: __webpack_require__(/*! ./app-home.component.html */ "./src/app/app-home/app-home.component.html"),
            styles: [__webpack_require__(/*! ./app-home.component.scss */ "./src/app/app-home/app-home.component.scss")],
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], AppHomeComponent);
    return AppHomeComponent;
}());



/***/ }),

/***/ "./src/app/app-home/item-swiper/item-swiper.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/app-home/item-swiper/item-swiper.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<swiper class=\"swiper-container\" [config]=\"swiperMessage\" [(index)]=\"index\">\r\n    <div class=\"swiper-slide\">\r\n        <div class=\"item-box\">\r\n          <div class=\"img-box\">\r\n            <img src=\"./assets/img/na4.png\" alt=\"\" class=\"img-fluid mx-auto d-block\">\r\n            <div class=\"action tl\">\r\n              <div class=\"action-item\">\r\n                <a href=\"\"><img src=\"./assets/img/i-like.png\"></a>\r\n              </div>\r\n               <div class=\"action-item\">\r\n                 <a href=\"\"><img src=\"./assets/img/i-full-size.png\"></a>\r\n               </div>\r\n            </div>\r\n            <div class=\"action bc\">\r\n              <button type=\"button\" class=\"btn btn-pri\" routerLink=\"/products\">Shop Now</button>\r\n            </div>\r\n          </div>\r\n          <div class=\"info-box text-center\">\r\n            <h1>Red Lipstick</h1>{{index}}\r\n            <p>$49.00</p>\r\n          </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"swiper-slide\">\r\n        <div class=\"item-box\">\r\n          <div class=\"img-box\">\r\n            <img src=\"./assets/img/na2.png\" alt=\"\" class=\"img-fluid mx-auto d-block\">\r\n            <div class=\"action tl\">\r\n              <div class=\"action-item\">\r\n                <a href=\"\"><img src=\"./assets/img/i-like.png\"></a>\r\n              </div>\r\n               <div class=\"action-item\">\r\n                 <a href=\"\"><img src=\"./assets/img/i-full-size.png\"></a>\r\n               </div>\r\n            </div>\r\n            <div class=\"action bc\">\r\n              <button type=\"button\" class=\"btn btn-pri\" routerLink=\"/products\">Shop Now</button>\r\n            </div>\r\n          </div>\r\n          <div class=\"info-box text-center\">\r\n            <h1>Serum</h1>\r\n            <p>$79.00</p>\r\n          </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"swiper-slide\">\r\n        <div class=\"item-box\">\r\n          <div class=\"img-box\">\r\n            <img src=\"./assets/img/na3.png\" alt=\"\" class=\"img-fluid mx-auto d-block\">\r\n            <div class=\"action tl\">\r\n              <div class=\"action-item\">\r\n                <a href=\"\"><img src=\"./assets/img/i-like.png\"></a>\r\n              </div>\r\n               <div class=\"action-item\">\r\n                 <a href=\"\"><img src=\"./assets/img/i-full-size.png\"></a>\r\n               </div>\r\n            </div>\r\n            <div class=\"action bc\">\r\n              <button type=\"button\" class=\"btn btn-pri\" routerLink=\"/products\">Shop Now</button>\r\n            </div>\r\n          </div>\r\n          <div class=\"info-box text-center\">\r\n            <h1>Lipstick</h1>\r\n            <p><del>$65.00</del>$39.99</p>\r\n          </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"swiper-slide\">\r\n        <div class=\"item-box\">\r\n          <div class=\"img-box\">\r\n            <img src=\"./assets/img/na5.png\" alt=\"\" class=\"img-fluid mx-auto d-block\">\r\n            <div class=\"action tl\">\r\n              <div class=\"action-item\">\r\n                <a href=\"\"><img src=\"./assets/img/i-like.png\"></a>\r\n              </div>\r\n               <div class=\"action-item\">\r\n                 <a href=\"\"><img src=\"./assets/img/i-full-size.png\"></a>\r\n               </div>\r\n            </div>\r\n            <div class=\"action bc\">\r\n              <button type=\"button\" class=\"btn btn-pri\" routerLink=\"/products\">Shop Now</button>\r\n            </div>\r\n          </div>\r\n          <div class=\"info-box text-center\">\r\n            <h1>Serum</h1>\r\n            <p>$79.00</p>\r\n          </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"swiper-slide\">\r\n        <div class=\"item-box\">\r\n          <div class=\"img-box\">\r\n            <img src=\"./assets/img/na6.png\" alt=\"\" class=\"img-fluid mx-auto d-block\">\r\n            <div class=\"action tl\">\r\n              <div class=\"action-item\">\r\n                <a href=\"\"><img src=\"./assets/img/i-like.png\"></a>\r\n              </div>\r\n               <div class=\"action-item\">\r\n                 <a href=\"\"><img src=\"./assets/img/i-full-size.png\"></a>\r\n               </div>\r\n            </div>\r\n            <div class=\"action bc\">\r\n              <button type=\"button\" class=\"btn btn-pri\" routerLink=\"/products\">Shop Now</button>\r\n            </div>\r\n          </div>\r\n          <div class=\"info-box text-center\">\r\n            <h1>Serum</h1>\r\n            <p>$79.00</p>\r\n          </div>\r\n        </div>\r\n    </div>\r\n</swiper>"

/***/ }),

/***/ "./src/app/app-home/item-swiper/item-swiper.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/app-home/item-swiper/item-swiper.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".item-box {\n  position: relative;\n  box-shadow: 0 1px 2px 0 rgba(110, 110, 110, 0.302), 0 1px 3px 1px rgba(255, 255, 255, 0.149); }\n  .item-box:hover .img-box {\n    border: 1px solid #ddd; }\n  .item-box:hover .img-box .action {\n    display: block; }\n  .item-box .img-box {\n    position: relative;\n    min-height: 350px; }\n  @media (min-width: 768px) and (max-width: 991px) {\n      .item-box .img-box {\n        min-height: 250px; } }\n  @media (max-width: 767px) {\n      .item-box .img-box {\n        min-height: 250px; } }\n  .item-box .img-box .action {\n      display: none;\n      position: absolute; }\n  .item-box .img-box .action .action-item {\n        border-width: 1px;\n        border-color: #dddddd;\n        border-style: solid;\n        background-color: white;\n        padding: 12px; }\n  .item-box .img-box .action .btn-pri {\n        background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n        box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n        font-size: 14px;\n        color: #000;\n        font-weight: 700;\n        padding: 16px 40px;\n        font-family: \"Libre Baskerville\", serif;\n        border-radius: 0;\n        border-top: 2px solid transparent; }\n  @media (min-width: 768px) and (max-width: 991px) {\n          .item-box .img-box .action .btn-pri {\n            padding: 12px 20px; } }\n  @media (max-width: 767px) {\n          .item-box .img-box .action .btn-pri {\n            padding: 16px 22px; } }\n  .item-box .img-box .action .btn-pri:hover {\n        box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.6);\n        border-top: 2px solid #c6755d;\n        color: #000 !important; }\n  .item-box .img-box .tl {\n      right: 0;\n      top: 0; }\n  .item-box .img-box .bc {\n      bottom: 20px;\n      left: 0;\n      right: 0;\n      text-align: center; }\n  .item-box .info-box h1 {\n    font-size: 14px;\n    font-weight: 400;\n    font-family: \"Libre Franklin\", sans-serif;\n    color: #000;\n    margin: 15px 0; }\n  .item-box .info-box h1:hover {\n      color: #cfa092; }\n  .item-box .info-box p {\n    font-size: 16px;\n    font-weight: 600;\n    font-family: \"Libre Franklin\", sans-serif;\n    color: #000;\n    padding-bottom: 8px; }\n  .item-box .info-box p del {\n      font-size: 16px;\n      font-weight: 400;\n      font-family: \"Libre Franklin\", sans-serif;\n      color: #000;\n      padding-right: 10px; }\n"

/***/ }),

/***/ "./src/app/app-home/item-swiper/item-swiper.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/app-home/item-swiper/item-swiper.component.ts ***!
  \***************************************************************/
/*! exports provided: ItemSwiperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemSwiperComponent", function() { return ItemSwiperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ItemSwiperComponent = /** @class */ (function () {
    function ItemSwiperComponent() {
    }
    ItemSwiperComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ItemSwiperComponent.prototype, "swiperMessage", void 0);
    ItemSwiperComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-item-swiper',
            template: __webpack_require__(/*! ./item-swiper.component.html */ "./src/app/app-home/item-swiper/item-swiper.component.html"),
            styles: [__webpack_require__(/*! ./item-swiper.component.scss */ "./src/app/app-home/item-swiper/item-swiper.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ItemSwiperComponent);
    return ItemSwiperComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./team-list.resolver */ "./src/app/team-list.resolver.ts");
/* harmony import */ var _app_home_app_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-home/app-home.component */ "./src/app/app-home/app-home.component.ts");
/* harmony import */ var _product_product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./product/product-listing/product-listing.component */ "./src/app/product/product-listing/product-listing.component.ts");
/* harmony import */ var _product_product_details_product_details_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product/product-details/product-details.component */ "./src/app/product/product-details/product-details.component.ts");
/* harmony import */ var _product_cart_products_cart_products_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./product/cart-products/cart-products.component */ "./src/app/product/cart-products/cart-products.component.ts");
/* harmony import */ var _product_wishlist_wishlist_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./product/wishlist/wishlist.component */ "./src/app/product/wishlist/wishlist.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    {
        path: 'products',
        component: _product_product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_4__["ProductListingComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    },
    {
        path: 'product-details',
        component: _product_product_details_product_details_component__WEBPACK_IMPORTED_MODULE_5__["ProductDetailsComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    },
    {
        path: 'cart',
        component: _product_cart_products_cart_products_component__WEBPACK_IMPORTED_MODULE_6__["CartProductsComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    },
    {
        path: 'wishlist',
        component: _product_wishlist_wishlist_component__WEBPACK_IMPORTED_MODULE_7__["WishlistComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    },
    {
        path: '',
        component: _app_home_app_home_component__WEBPACK_IMPORTED_MODULE_3__["AppHomeComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    },
    {
        path: '**',
        redirectTo: '/'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-app-header></app-app-header>\r\n<app-search></app-search>\r\n<router-outlet></router-outlet>\r\n<app-app-footer></app-app-footer>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bottom-filter {\n  position: fixed;\n  bottom: 20px;\n  width: 100%; }\n  .bottom-filter mat-card {\n    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.5);\n    background-color: #fff; }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"])) {
                return;
            }
            window.scrollTo(0, 0);
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var angular_font_awesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-font-awesome */ "./node_modules/angular-font-awesome/dist/angular-font-awesome.es5.js");
/* harmony import */ var ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-swiper-wrapper */ "./node_modules/ngx-swiper-wrapper/dist/ngx-swiper-wrapper.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/bottom-sheet */ "./node_modules/@angular/material/esm5/bottom-sheet.es5.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var ngx_image_zoom__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-image-zoom */ "./node_modules/ngx-image-zoom/ngx-image-zoom.umd.js");
/* harmony import */ var ngx_image_zoom__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(ngx_image_zoom__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _app_header_app_header_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./app-header/app-header.component */ "./src/app/app-header/app-header.component.ts");
/* harmony import */ var _app_footer_app_footer_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./app-footer/app-footer.component */ "./src/app/app-footer/app-footer.component.ts");
/* harmony import */ var _app_home_app_home_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./app-home/app-home.component */ "./src/app/app-home/app-home.component.ts");
/* harmony import */ var _product_product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./product/product-listing/product-listing.component */ "./src/app/product/product-listing/product-listing.component.ts");
/* harmony import */ var _app_home_item_swiper_item_swiper_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./app-home/item-swiper/item-swiper.component */ "./src/app/app-home/item-swiper/item-swiper.component.ts");
/* harmony import */ var _product_product_details_product_details_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./product/product-details/product-details.component */ "./src/app/product/product-details/product-details.component.ts");
/* harmony import */ var _product_cart_calculator_cart_calculator_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./product/cart-calculator/cart-calculator.component */ "./src/app/product/cart-calculator/cart-calculator.component.ts");
/* harmony import */ var _product_cart_products_cart_products_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./product/cart-products/cart-products.component */ "./src/app/product/cart-products/cart-products.component.ts");
/* harmony import */ var _product_checkout_checkout_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./product/checkout/checkout.component */ "./src/app/product/checkout/checkout.component.ts");
/* harmony import */ var _static_pages_static_pages_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./static-pages/static-pages.module */ "./src/app/static-pages/static-pages.module.ts");
/* harmony import */ var _account_account_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./account/account.module */ "./src/app/account/account.module.ts");
/* harmony import */ var _search_search_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./search/search.component */ "./src/app/search/search.component.ts");
/* harmony import */ var _product_wishlist_wishlist_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./product/wishlist/wishlist.component */ "./src/app/product/wishlist/wishlist.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _app_header_app_header_component__WEBPACK_IMPORTED_MODULE_13__["AppHeaderComponent"],
                _app_footer_app_footer_component__WEBPACK_IMPORTED_MODULE_14__["AppFooterComponent"],
                _app_home_app_home_component__WEBPACK_IMPORTED_MODULE_15__["AppHomeComponent"],
                _app_home_item_swiper_item_swiper_component__WEBPACK_IMPORTED_MODULE_17__["ItemSwiperComponent"],
                _product_product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_16__["ProductListingComponent"],
                _product_product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_16__["ProductListingDialogComponent"],
                _product_product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_16__["ProductListingSortDialogComponent"],
                _product_product_details_product_details_component__WEBPACK_IMPORTED_MODULE_18__["ProductDetailsComponent"],
                _product_cart_calculator_cart_calculator_component__WEBPACK_IMPORTED_MODULE_19__["CartCalculatorComponent"],
                _product_cart_products_cart_products_component__WEBPACK_IMPORTED_MODULE_20__["CartProductsComponent"],
                _product_checkout_checkout_component__WEBPACK_IMPORTED_MODULE_21__["CheckoutComponent"],
                _search_search_component__WEBPACK_IMPORTED_MODULE_24__["SearchComponent"],
                _product_wishlist_wishlist_component__WEBPACK_IMPORTED_MODULE_25__["WishlistComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
                _account_account_module__WEBPACK_IMPORTED_MODULE_23__["AccountModule"],
                _static_pages_static_pages_module__WEBPACK_IMPORTED_MODULE_22__["StaticPagesModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatTabsModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_8__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__["MatDialogModule"],
                _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_10__["MatBottomSheetModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_11__["MatListModule"],
                angular_font_awesome__WEBPACK_IMPORTED_MODULE_5__["AngularFontAwesomeModule"],
                ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_6__["SwiperModule"],
                ngx_image_zoom__WEBPACK_IMPORTED_MODULE_12__["NgxImageZoomModule"].forRoot()
            ],
            entryComponents: [_product_product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_16__["ProductListingDialogComponent"], _product_product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_16__["ProductListingSortDialogComponent"]],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/product/cart-calculator/cart-calculator.component.html":
/*!************************************************************************!*\
  !*** ./src/app/product/cart-calculator/cart-calculator.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"cart-summary\">\r\n\t<h1>Order Summary</h1>\r\n\t<div class=\"summary-wrap\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8\">\r\n\t\t\t\t<p class=\"summary-list\">Bag Total</p>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4\">\r\n\t\t\t\t<p class=\"summary-list text-right\">$643</p>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8\">\r\n\t\t\t\t<p class=\"summary-list\">Bag Discount</p>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4\">\r\n\t\t\t\t<p class=\"summary-list text-right\">$20</p>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8\">\r\n\t\t\t\t<p class=\"summary-list\">Delivery Charges</p>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4\">\r\n\t\t\t\t<p class=\"summary-list txt-green text-right\">Free</p>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t\t\t\t<div class=\"apply-coupon input-action-wrap\" (click)=\"couponCode = !couponCode\">\r\n\t\t\t\t\t<p>\r\n\t\t\t\t\t\t<img src=\"./assets/img/coupon.jpg\"> Apply Coupon \r\n\t\t\t\t\t\t<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>\r\n\t\t\t\t\t</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"row\" *ngIf=\"couponCode\">\r\n\t\t\t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t\t\t\t<div class=\"input-coupon\">\r\n\t\t\t\t\t<form>\r\n\t\t\t\t\t\t<div class=\"input-group mb-3\">\r\n\t\t\t\t\t\t  <input type=\"text\" class=\"form-control\" placeholder=\"Coupon Code\">\r\n\t\t\t\t\t\t  <div class=\"input-group-append\">\r\n\t\t\t\t\t\t    <button class=\"btn btn-outline-dark\" type=\"submit\">Apply</button>\r\n\t\t\t\t\t\t  </div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</form>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t\t\t\t<div class=\"apply-coupon\">\r\n\t\t\t\t\t<p>\r\n\t\t\t\t\t\t<img src=\"./assets/img/coupon.jpg\"> BDUK38 \r\n\t\t\t\t\t\t<span class=\"apply-btn\">Applied</span>\r\n\t\t\t\t\t</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8\">\r\n\t\t\t\t<p class=\"summary-list\">Coupon Discount</p>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4\">\r\n\t\t\t\t<p class=\"summary-list txt-green text-right\">$50</p>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"total-sum\">\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8\">\r\n\t\t\t\t\t<p class=\"total\">Amount Payable</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4\">\r\n\t\t\t\t\t<p class=\"total text-right\">$573</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n    <div class=\"text-center mt-3\">\r\n    \t<button class=\"btn btn-po\" routerLink=\"/\">Place Order</button>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/product/cart-calculator/cart-calculator.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/product/cart-calculator/cart-calculator.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".cart-summary h1 {\n  font-size: 18px;\n  color: #cfa092;\n  font-family: \"Libre Baskerville\", serif;\n  margin-bottom: 20px; }\n\n.cart-summary .summary-wrap {\n  border-radius: 12px;\n  background-color: white;\n  box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.25);\n  padding: 24px; }\n\n.cart-summary .summary-wrap .summary-list {\n    font-size: 16px;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif; }\n\n.cart-summary .summary-wrap .summary-list.txt-green {\n      color: #03a685; }\n\n.cart-summary .summary-wrap .total-sum {\n    border-top: 1px solid #dddddd; }\n\n.cart-summary .summary-wrap .total-sum .total {\n      font-size: 16px;\n      color: #000;\n      font-weight: 700;\n      font-family: \"Libre Franklin\", sans-serif;\n      padding: 20px 0 0;\n      margin: 0; }\n\n.cart-summary .summary-wrap .apply-coupon {\n    border-width: 1px;\n    border-color: #cfa092;\n    border-style: dashed;\n    padding: 12px 20px;\n    background-color: #f4eae8;\n    margin-bottom: 16px; }\n\n.cart-summary .summary-wrap .apply-coupon.input-action-wrap {\n      cursor: pointer; }\n\n.cart-summary .summary-wrap .apply-coupon p {\n      margin: 0;\n      font-size: 14px;\n      color: #000;\n      font-weight: 400;\n      font-family: \"Libre Franklin\", sans-serif; }\n\n.cart-summary .summary-wrap .apply-coupon p img {\n        padding-right: 6px; }\n\n.cart-summary .summary-wrap .apply-coupon p .fa {\n        font-size: 22px;\n        float: right; }\n\n.cart-summary .summary-wrap .apply-coupon p .apply-btn {\n        font-size: 14px;\n        color: #cfa092;\n        float: right;\n        font-weight: 700;\n        font-family: \"Libre Franklin\", sans-serif; }\n\n.cart-summary .btn-po {\n  background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n  box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n  margin-right: 20px;\n  font-size: 14px;\n  font-weight: 700;\n  color: #000;\n  font-family: \"Libre Baskerville\", serif;\n  padding: 14px 40px;\n  border-radius: 0;\n  border-top: 2px solid transparent; }\n\n.cart-summary .btn-po:hover, .cart-summary .btn-po:focus {\n    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.6);\n    border-top: 2px solid #c6755d; }\n"

/***/ }),

/***/ "./src/app/product/cart-calculator/cart-calculator.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/product/cart-calculator/cart-calculator.component.ts ***!
  \**********************************************************************/
/*! exports provided: CartCalculatorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartCalculatorComponent", function() { return CartCalculatorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CartCalculatorComponent = /** @class */ (function () {
    function CartCalculatorComponent() {
        this.couponCode = false;
    }
    CartCalculatorComponent.prototype.ngOnInit = function () {
    };
    CartCalculatorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cart-calculator',
            template: __webpack_require__(/*! ./cart-calculator.component.html */ "./src/app/product/cart-calculator/cart-calculator.component.html"),
            styles: [__webpack_require__(/*! ./cart-calculator.component.scss */ "./src/app/product/cart-calculator/cart-calculator.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CartCalculatorComponent);
    return CartCalculatorComponent;
}());



/***/ }),

/***/ "./src/app/product/cart-products/cart-products.component.html":
/*!********************************************************************!*\
  !*** ./src/app/product/cart-products/cart-products.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"page-wrap mb-5\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7\">\r\n      \t<div class=\"cart-items\">\r\n      \t\t<h1>My Shopping Bag (4 Items)</h1>\r\n            <ng-container *ngFor=\"let item of cart\">\r\n            \t<div class=\"items\" *ngIf=\"item.id\">\r\n            \t\t<div class=\"row\">\r\n            \t\t\t<div class=\"col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9\">\r\n            \t\t\t\t<div class=\"media\">\r\n\t\t\t\t\t\t\t\t<img src=\"{{item.image}}\" class=\"align-self-center mr-3 p-2\">\r\n\t\t\t\t\t\t\t\t<div class=\"media-body align-self-center\">\r\n\t\t\t\t\t\t\t\t    <h2>{{item.name}}</h2>\r\n\t\t\t\t\t\t\t\t    <p>{{item.brand}}</p>\r\n\t\t\t\t\t\t\t\t    <div class=\"add-more\">\r\n\t\t\t\t\t\t\t\t    \t<form class=\"form-inline\">\r\n\t\t\t\t\t\t\t\t    \t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t    \t\t\t<label for=\"Size\">Size</label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<option>250 ml</option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<option>300 ml</option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<option>350 ml</option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<option>500 ml</option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</select>\r\n\t\t\t\t\t\t\t\t    \t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"Qty\">Qty</label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<option>1</option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<option>2</option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<option>3</option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<option>4</option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</select>\r\n\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t    </form>\r\n\t\t\t\t\t\t\t\t    </div>\r\n\t\t\t\t\t\t\t\t    <div class=\"d-block d-lg-none\">\r\n\t\t\t\t\t\t\t\t\t    <p class=\"price\">{{item.price | currency:'USD' :'symbol':'3.0'}}</p>\r\n\t\t\t            \t\t\t\t<p *ngIf=\"item.offrs!==0\">\r\n\t\t\t            \t\t\t\t\t<del>{{item.offrs | currency:'USD' :'symbol':'3.0'}}</del>\r\n\t\t\t            \t\t\t\t\t<span>{{item.offper}}</span>\r\n\t\t\t            \t\t\t\t</p>\r\n\t\t            \t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t    </div>\r\n            \t\t\t</div>\r\n            \t\t\t<div class=\"col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 align-self-center d-none d-lg-block\">\r\n            \t\t\t\t<p class=\"price\">{{item.price | currency:'USD' :'symbol':'3.0'}}</p>\r\n            \t\t\t\t<p *ngIf=\"item.offrs!==0\">\r\n            \t\t\t\t\t<del>{{item.offrs | currency:'USD' :'symbol':'3.0'}}</del>\r\n            \t\t\t\t\t<span>{{item.offper}}</span>\r\n            \t\t\t\t</p>\r\n            \t\t\t</div>\r\n            \t\t</div>\r\n            \t\t<div class=\"row\">\r\n            \t\t\t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n            \t\t\t\t<div class=\"item-action\">\r\n            \t\t\t\t\t<div class=\"row\">\r\n\t            \t\t\t\t    <div class=\"col text-center divider\">\r\n\t\t\t            \t\t\t\t<div class=\"item-action-wrap\">\r\n\t\t\t            \t\t\t\t\t<button class=\"btn\">\r\n\t\t\t            \t\t\t\t\t<img src=\"./assets/img/remove.jpg\" alt=\"Remove Button\" title=\"Remove\">Remove</button>\r\n\t\t\t            \t\t\t\t</div>\r\n\t\t\t            \t\t\t</div>\r\n\t\t\t            \t\t\t<div class=\"col text-center\">\r\n\t\t\t            \t\t\t    <div class=\"item-action-wrap\">\r\n\t\t\t            \t\t\t\t\t<button class=\"btn\">\r\n\t\t\t            \t\t\t\t\t<img src=\"./assets/img/move.jpg\" alt=\"Wishlist Button\" title=\"Move to Wishlist\">Move to Wishlist</button>\r\n\t\t\t            \t\t\t\t</div>\r\n\t\t\t            \t\t\t</div>\r\n\t            \t\t\t\t</div>\r\n            \t\t\t\t</div>\r\n            \t\t\t</div>\r\n            \t\t</div>\r\n            \t</div>\r\n            </ng-container>\r\n      \t</div>\r\n      </div>\r\n      <div class=\"col-12 col-sm-12 col-md-5 offset-lg-1 col-lg-4 offset-xl-1 col-xl-4\">\r\n      \t<app-cart-calculator></app-cart-calculator>\r\n      </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/product/cart-products/cart-products.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/product/cart-products/cart-products.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".cart-items h1 {\n  font-size: 18px;\n  color: #cfa092;\n  font-family: \"Libre Baskerville\", serif;\n  margin-bottom: 20px; }\n\n.cart-items .items {\n  box-shadow: 0 1px 2px 0 rgba(60, 64, 67, 0.302), 0 1px 3px 1px rgba(60, 64, 67, 0.149);\n  border-radius: 6px;\n  background-color: white;\n  margin-bottom: 22px; }\n\n.cart-items .items .media h2 {\n    font-size: 14px;\n    color: #000;\n    font-family: \"Libre Baskerville\", serif;\n    padding-top: 12px; }\n\n.cart-items .items .media p {\n    font-size: 12px;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif;\n    margin-bottom: 24px; }\n\n.cart-items .items .media label {\n    font-size: 14px;\n    color: #666;\n    font-weight: 600;\n    font-family: \"Libre Franklin\", sans-serif;\n    padding-right: 6px; }\n\n.cart-items .items .media .form-group {\n    margin: 0 8px 0 0; }\n\n@media (max-width: 320px) {\n      .cart-items .items .media .form-group {\n        width: 100%; } }\n\n.cart-items .items .media select {\n    font-size: 12px;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif;\n    height: 38px; }\n\n.cart-items .items .media .price {\n    font-size: 24px;\n    color: #cfa092;\n    font-weight: 600;\n    font-family: \"Libre Baskerville\", serif;\n    margin-bottom: 6px; }\n\n.cart-items .items .media del {\n    font-size: 14px;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif; }\n\n.cart-items .items .media span {\n    font-size: 14px;\n    color: #03a685;\n    font-family: \"Libre Franklin\", sans-serif;\n    padding-left: 10px; }\n\n.cart-items .items .media .add-more {\n    margin: 0 0 10px; }\n\n.cart-items .items .price {\n    font-size: 24px;\n    color: #cfa092;\n    font-weight: 600;\n    font-family: \"Libre Baskerville\", serif;\n    margin-bottom: 6px; }\n\n.cart-items .items del {\n    font-size: 14px;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif; }\n\n.cart-items .items span {\n    font-size: 14px;\n    color: #03a685;\n    font-family: \"Libre Franklin\", sans-serif;\n    padding-left: 10px; }\n\n.cart-items .items .item-action {\n    border-top: 1px solid #dddddd; }\n\n.cart-items .items .item-action .divider {\n      border-right: 1px solid #ddd; }\n\n.cart-items .items .item-action .btn {\n      font-size: 14px;\n      color: #000;\n      font-family: \"Libre Baskerville\", serif;\n      padding: 12px 0; }\n\n.cart-items .items .item-action .btn img {\n        padding-right: 10px; }\n\n@media (max-width: 767px) {\n          .cart-items .items .item-action .btn img {\n            display: none; } }\n"

/***/ }),

/***/ "./src/app/product/cart-products/cart-products.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/product/cart-products/cart-products.component.ts ***!
  \******************************************************************/
/*! exports provided: CartProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartProductsComponent", function() { return CartProductsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CartProductsComponent = /** @class */ (function () {
    function CartProductsComponent() {
        this.cart = [
            { id: 1,
                image: "./assets/img/c1.jpg",
                name: "The Body Shop : Body Shampoo",
                brand: "Unisex Strawberry Shower Gel",
                offrs: 0,
                offper: "",
                price: 79
            },
            { id: 2,
                image: "./assets/img/c2.jpg",
                name: "M.A.C Lipstick",
                brand: "Berry Recognize Mega Last Matte Lipstick D926B",
                offrs: 200,
                offper: "10% Off",
                price: 180
            },
            { id: 3,
                image: "./assets/img/c3.jpg",
                name: "Maybelline Foundation",
                brand: "NC37 Studio Fix Fluid Foundation SPF 15",
                offrs: 0,
                offper: "",
                price: 215
            },
            { id: 4,
                image: "./assets/img/c4.jpg",
                name: "Biotique : Body Wash",
                brand: "Orange Shower Gel",
                offrs: 0,
                offper: "",
                price: 149
            }
        ];
    }
    CartProductsComponent.prototype.ngOnInit = function () {
    };
    CartProductsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cart-products',
            template: __webpack_require__(/*! ./cart-products.component.html */ "./src/app/product/cart-products/cart-products.component.html"),
            styles: [__webpack_require__(/*! ./cart-products.component.scss */ "./src/app/product/cart-products/cart-products.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CartProductsComponent);
    return CartProductsComponent;
}());



/***/ }),

/***/ "./src/app/product/checkout/checkout.component.html":
/*!**********************************************************!*\
  !*** ./src/app/product/checkout/checkout.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  checkout works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/product/checkout/checkout.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/product/checkout/checkout.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/product/checkout/checkout.component.ts":
/*!********************************************************!*\
  !*** ./src/app/product/checkout/checkout.component.ts ***!
  \********************************************************/
/*! exports provided: CheckoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutComponent", function() { return CheckoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CheckoutComponent = /** @class */ (function () {
    function CheckoutComponent() {
    }
    CheckoutComponent.prototype.ngOnInit = function () {
    };
    CheckoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-checkout',
            template: __webpack_require__(/*! ./checkout.component.html */ "./src/app/product/checkout/checkout.component.html"),
            styles: [__webpack_require__(/*! ./checkout.component.scss */ "./src/app/product/checkout/checkout.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CheckoutComponent);
    return CheckoutComponent;
}());



/***/ }),

/***/ "./src/app/product/product-details/product-details.component.html":
/*!************************************************************************!*\
  !*** ./src/app/product/product-details/product-details.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"page-wrap\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n      \t<ul class=\"breadcrumb\">\r\n\t\t\t\t  <li class=\"breadcrumb-item\"><a routerLink=\"/\">Home</a></li>\r\n\t\t\t\t  <li class=\"breadcrumb-item\"><a routerLink=\"/products\">Shop</a></li>\r\n\t\t\t\t  <li class=\"breadcrumb-item active\">Body Shampoo</li>\r\n\t\t\t\t</ul>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6\">\r\n      \t<div class=\"full-img pt-4\">\r\n          <ngx-image-zoom\r\n            [thumbImage]=myThumbnail\r\n            [fullImage]=myFullresImage\r\n            [zoomMode]=\"hover\">\r\n          </ngx-image-zoom>\r\n       \t</div>\r\n       \t<div class=\"thumb-img pt-2\">\r\n          <swiper class=\"swiper-container\" [config]=\"thumbConfig\" [(index)]=\"index\">\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"thumb-img-item mr-1\">\r\n                <img src=\"./assets/img/pds1.png\" class=\"img-fluid\" (click)=\"myThumbnail='./assets/img/pd.png'; myFullresImage='./assets/img/pdf.png'\">\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"thumb-img-item mr-1\">\r\n                <img src=\"./assets/img/pds2.png\" class=\"img-fluid\" (click)=\"myThumbnail='./assets/img/pd2.jpg'; myFullresImage='./assets/img/pd2f.jpg'\">\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"thumb-img-item mr-1\">\r\n                <img src=\"./assets/img/pds1.png\" class=\"img-fluid\" (click)=\"myThumbnail='./assets/img/pd.png'; myFullresImage='./assets/img/pdf.png'\">\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"thumb-img-item mr-1\">\r\n                <img src=\"./assets/img/pds2.png\" class=\"img-fluid\" (click)=\"myThumbnail='./assets/img/pd2.jpg'; myFullresImage='./assets/img/pd2f.jpg'\">\r\n              </div>\r\n            </div>\r\n            <div class=\"swiper-slide\">\r\n              <div class=\"thumb-img-item mr-1\">\r\n                <img src=\"./assets/img/pds1.png\" class=\"img-fluid\" (click)=\"myThumbnail='./assets/img/pd.png'; myFullresImage='./assets/img/pdf.png'\">\r\n              </div>\r\n            </div>\r\n          </swiper>\r\n       \t</div>\r\n      </div>\r\n      <div class=\"col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-5 offset-xl-1 col-xl-5\">\r\n        <div class=\"product-details-wrap pt-4\">\r\n       \t  <h1>The Body Shop : Body Shampoo</h1>\r\n       \t  <p class=\"brand\">Unisex Strawberry Shower Gel</p>\r\n       \t  <div class=\"reviews\"> \r\n       \t  \t<i class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n       \t  \t<i class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n       \t  \t<i class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n       \t  \t<i class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n       \t  \t<i class=\"fa fa-star-half-o\" aria-hidden=\"true\"></i>\r\n       \t  \t<span class=\"star-count\">4.5</span>\r\n       \t  \t<span class=\"reviews-count\">178 Reviews</span>\r\n       \t  </div>\r\n       \t  <div class=\"price\">\r\n       \t  \t<p>$79</p>\r\n       \t  </div>\r\n       \t  <div class=\"select-option\">\r\n       \t  \t<p class=\"head\">Select size:</p>\r\n       \t  \t<div>\r\n       \t  \t\t<button class=\"btn btn-size active\">250 ml</button>\r\n       \t  \t\t<button class=\"btn btn-size\">120 ml</button>\r\n       \t  \t\t<button class=\"btn btn-size\">55 ml</button>\r\n       \t  \t</div>\r\n       \t  </div>\r\n       \t  <div class=\"action-btn\">\r\n       \t  \t<button class=\"btn btn-atc\" routerLink=\"/cart\">Add to Cart</button>\r\n       \t  \t<button class=\"btn btn-atw\">Add to Wishlist</button>\r\n       \t  \t<p>This item cannot be returned.</p>\r\n       \t  </div>\r\n       \t  <div class=\"extra-details\">\r\n       \t  \t<h3>PRODUCT DETAILS</h3>\r\n       \t  \t<ul>\r\n       \t  \t\t<li><p>This soap-free shower gel contains real strawberry seed oil</p></li>\r\n       \t  \t\t<li><p>Soap-free</p></li>\r\n       \t  \t\t<li><p>Lather-rich</p></li>\r\n       \t  \t\t<li><p>Sweet strawberry scent</p></li>\r\n       \t  \t</ul>\r\n       \t  \t<p class=\"sub-head\">Specifications</p>\r\n       \t  \t<ul>\r\n       \t  \t\t<li><p><span>Type :</span> Body Wash</p></li>\r\n       \t  \t\t<li><p><span>Formulation :</span>Liquid</p></li>\r\n       \t  \t</ul>\r\n       \t  \t<p class=\"sub-head\">Product Code</p>\r\n       \t  \t<ul>\r\n       \t  \t\t<li><p>1672012</p></li>\r\n       \t  \t</ul>\r\n       \t  </div>\r\n        </div>\r\n\t\t  </div>\r\n\t\t</div>\r\n\t</div>\r\n</section>\r\n<section class=\"similar-products ptb-40 mt-5 mb-5\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <h2 class=\"text-center section-head\">Similar Products</h2>\r\n        <div class=\"text-center\">\r\n          <img alt=\"Title\" src=\"./assets/img/title-icon.png\" class=\"\">\r\n        </div>\r\n        <p class=\"text-center section-sub-head\">View similar products</p>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n        <app-item-swiper [swiperMessage]=\"similarConfig\"></app-item-swiper>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n"

/***/ }),

/***/ "./src/app/product/product-details/product-details.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/product/product-details/product-details.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".breadcrumb {\n  background-color: transparent;\n  margin-top: 0;\n  margin-bottom: 0;\n  padding: 0; }\n  .breadcrumb a {\n    font-size: 14px;\n    font-weight: 400;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .breadcrumb .active {\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .product-details-wrap h1 {\n  font-size: 24px;\n  font-weight: 400;\n  color: #000;\n  font-family: \"Libre Baskerville\", serif;\n  margin-bottom: 14px; }\n  .product-details-wrap .brand {\n  font-size: 18px;\n  font-weight: 400;\n  color: #666666;\n  font-family: \"Libre Franklin\", sans-serif;\n  margin-bottom: 20px; }\n  .product-details-wrap .reviews {\n  margin-bottom: 20px; }\n  .product-details-wrap .reviews .fa {\n    padding: 0 6px 0 0; }\n  .product-details-wrap .reviews .star-count {\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif;\n    padding: 0 10px; }\n  .product-details-wrap .reviews .reviews-count {\n    font-size: 14px;\n    font-weight: 400;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif;\n    padding: 0 20px; }\n  .product-details-wrap .price p {\n  color: #cfa092;\n  font-size: 30px;\n  font-family: \"Libre Baskerville\", serif;\n  font-weight: 700;\n  margin-bottom: 30px; }\n  .product-details-wrap .select-option {\n  margin-bottom: 30px; }\n  .product-details-wrap .select-option .head {\n    font-size: 14px;\n    font-weight: 700;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif;\n    margin-bottom: 14px; }\n  .product-details-wrap .select-option .btn-size {\n    font-size: 12px;\n    font-weight: 400;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif;\n    border: 1px solid #000;\n    margin: 0 8px 0 0;\n    border-radius: 16px; }\n  .product-details-wrap .select-option .btn-size.active {\n      background-color: #000;\n      color: #fff; }\n  .product-details-wrap .action-btn .btn-atc {\n  background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n  box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n  margin-right: 20px;\n  font-size: 14px;\n  font-weight: 700;\n  color: #000;\n  font-family: \"Libre Baskerville\", serif;\n  padding: 14px 28px;\n  border-radius: 0;\n  border-top: 2px solid transparent; }\n  .product-details-wrap .action-btn .btn-atc:hover {\n    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.6);\n    border-top: 2px solid #c6755d; }\n  @media (max-width: 767px) {\n    .product-details-wrap .action-btn .btn-atc {\n      margin-right: 14px;\n      padding: 14px 18px; } }\n  .product-details-wrap .action-btn .btn-atw {\n  background-image: linear-gradient(to right, #aaaaaa, #efefef, #cccccc);\n  box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n  font-size: 14px;\n  font-weight: 700;\n  color: #000;\n  font-family: \"Libre Baskerville\", serif;\n  padding: 14px 28px;\n  border-radius: 0;\n  border-top: 2px solid transparent; }\n  .product-details-wrap .action-btn .btn-atw:hover {\n    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.6);\n    border-top: 2px solid #aaa; }\n  @media (max-width: 767px) {\n    .product-details-wrap .action-btn .btn-atw {\n      padding: 14px 18px; } }\n  .product-details-wrap .action-btn p {\n  font-size: 12px;\n  font-weight: 400;\n  color: #666;\n  font-family: \"Libre Franklin\", sans-serif;\n  padding: 10px 0 24px;\n  border-bottom: 1px solid #ddd; }\n  .product-details-wrap .extra-details ul {\n  padding: 0;\n  list-style: none; }\n  .product-details-wrap .extra-details ul li {\n    font-size: 14px;\n    font-weight: 400;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .product-details-wrap .extra-details ul li p {\n      margin-bottom: 10px; }\n  .product-details-wrap .extra-details h3 {\n  font-size: 14px;\n  font-weight: 700;\n  padding-top: 10px;\n  color: #000;\n  text-transform: uppercase;\n  font-family: \"Libre Franklin\", sans-serif; }\n  .product-details-wrap .extra-details .sub-head {\n  font-size: 14px;\n  font-weight: 700;\n  padding-top: 10px;\n  color: #000;\n  font-family: \"Libre Franklin\", sans-serif; }\n  ::ng-deep .ngxImageZoomContainer > img {\n  max-height: 450px !important; }\n  @media (max-width: 767px) {\n    ::ng-deep .ngxImageZoomContainer > img {\n      max-height: 350px !important;\n      max-width: 350px !important; } }\n  @media (max-width: 420px) {\n    ::ng-deep .ngxImageZoomContainer > img {\n      max-height: 300px !important;\n      max-width: 300px !important; } }\n  @media (max-width: 320px) {\n    ::ng-deep .ngxImageZoomContainer > img {\n      max-height: 290px !important;\n      max-width: 290px !important; } }\n  @media (max-width: 767px) {\n  .thumb-img {\n    text-align: center; } }\n  .thumb-img .thumb-img-item {\n  display: inline-block; }\n"

/***/ }),

/***/ "./src/app/product/product-details/product-details.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/product/product-details/product-details.component.ts ***!
  \**********************************************************************/
/*! exports provided: ProductDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsComponent", function() { return ProductDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProductDetailsComponent = /** @class */ (function () {
    function ProductDetailsComponent() {
        this.myThumbnail = "./assets/img/pd.png";
        this.myFullresImage = "./assets/img/pdf.png";
        this.similarConfig = {
            direction: 'horizontal',
            slidesPerView: 4,
            keyboard: true,
            mousewheel: false,
            scrollbar: false,
            autoHeight: false,
            setWrapperSize: false,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            navigation: true,
            spaceBetween: 30,
            pagination: false,
            breakpoints: {
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                480: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                580: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 30
                }
            },
            observer: true
        };
        this.thumbConfig = {
            direction: 'horizontal',
            slidesPerView: 4,
            keyboard: true,
            loop: false,
            mousewheel: false,
            scrollbar: false,
            autoHeight: false,
            setWrapperSize: false,
            navigation: false,
            spaceBetween: 10,
            pagination: false,
            breakpoints: {
                320: {
                    slidesPerView: 2
                },
                480: {
                    slidesPerView: 3
                },
                580: {
                    slidesPerView: 3
                },
                768: {
                    slidesPerView: 4
                }
            }
        };
    }
    ProductDetailsComponent.prototype.ngOnInit = function () {
    };
    ProductDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-details',
            template: __webpack_require__(/*! ./product-details.component.html */ "./src/app/product/product-details/product-details.component.html"),
            styles: [__webpack_require__(/*! ./product-details.component.scss */ "./src/app/product/product-details/product-details.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProductDetailsComponent);
    return ProductDetailsComponent;
}());



/***/ }),

/***/ "./src/app/product/product-listing/product-listing-dialog.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/product/product-listing/product-listing-dialog.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-nav-list>\r\n  <div class=\"collection-filter\">\r\n    <div class=\"collection-filter-block\">\r\n      <mat-accordion multi=\"true\" >\r\n        <mat-expansion-panel [expanded]=\"true\">\r\n          <mat-expansion-panel-header>\r\n            <mat-panel-title>\r\n              Categories\r\n            </mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <div class=\"collection-brand-filter\">\r\n            <ul>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Lipstick<label>(786)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Perfume and Body Mist<label>(346)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Nail Polish<label>(811)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Foundation and Primer<label>(178)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Kajal and Eye Liner<label>(92)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Eyeshadow<label>(76)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Face Moisturisers<label>(109)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Shampoo and Conditioner<label>(67)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Make-up Brushes<label>(43)</label></mat-checkbox>\r\n              </li>\r\n              <p class=\"more\"><a href=\"#\">+ 9 more</a></p>\r\n            </ul>\r\n          </div>\r\n        </mat-expansion-panel>\r\n      \r\n        <mat-expansion-panel [expanded]=\"true\">\r\n          <mat-expansion-panel-header>\r\n            <mat-panel-title>\r\n              Brands\r\n            </mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <div class=\"collection-brand-filter\">\r\n            <ul>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">M.A.C<label>(786)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Colorbar<label>(346)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Lakme<label>(811)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Revlon<label>(178)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Maybelline<label>(92)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">LOreal<label>(76)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Lotus Herbals<label>(109)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Wet n Wild<label>(67)</label></mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">Biotique<label>(43)</label></mat-checkbox>\r\n              </li>\r\n              <p class=\"more\"><a href=\"#\">+ 9 more</a></p>\r\n            </ul>\r\n          </div>\r\n        </mat-expansion-panel>\r\n    \r\n        <mat-expansion-panel [expanded]=\"true\">\r\n          <mat-expansion-panel-header>\r\n            <mat-panel-title>\r\n              Discount\r\n            </mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <div class=\"collection-brand-filter\">\r\n            <ul>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">50% or more</mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">40% or more</mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">30% or more</mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">20% or more</mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">10% or more</mat-checkbox>\r\n              </li>\r\n              <li>\r\n                <mat-checkbox aria-label=\"\">10% or below</mat-checkbox>\r\n              </li>\r\n            </ul>\r\n          </div>\r\n        </mat-expansion-panel>\r\n      </mat-accordion>\r\n    </div>\r\n  </div>\r\n</mat-nav-list>"

/***/ }),

/***/ "./src/app/product/product-listing/product-listing-sort-dialog.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/product/product-listing/product-listing-sort-dialog.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-nav-list>\r\n  <div class=\"collection-filter\">\r\n    <div class=\"collection-filter-block\">\r\n    \t<mat-form-field class=\"d-block\">\r\n\t\t\t<mat-label>Sort by:</mat-label>\r\n\t\t\t  <mat-select>\r\n\t\t\t    <mat-option value=\"Popularity\">\r\n\t\t\t      Popularity\r\n\t\t\t    </mat-option>\r\n\t\t\t    <mat-option value=\"Top Rated\">\r\n\t\t\t      Top Rated\r\n\t\t\t    </mat-option>\r\n\t\t\t    <mat-option value=\"New\">\r\n\t\t\t      New\r\n\t\t\t    </mat-option>\r\n\t\t\t  </mat-select>\r\n\t\t\t</mat-form-field>\r\n    </div>\r\n  </div>\r\n</mat-nav-list>"

/***/ }),

/***/ "./src/app/product/product-listing/product-listing.component.html":
/*!************************************************************************!*\
  !*** ./src/app/product/product-listing/product-listing.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"page-wrap\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n      \t<ul class=\"breadcrumb\">\r\n\t\t\t\t  <li class=\"breadcrumb-item\"><a routerLink=\"/\">Home</a></li>\r\n\t\t\t\t  <li class=\"breadcrumb-item active\" routerLink=\"/products\">Shop</li>\r\n\t\t\t\t</ul>\r\n      </div>\r\n      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 d-none d-md-block\">\r\n      \t<div class=\"clearfix\">\r\n      \t\t<div class=\"float-right\">\r\n\t\t\t\t\t\t<mat-form-field>\r\n\t\t\t\t\t\t  <mat-label>Sort by:</mat-label>\r\n\t\t\t\t\t\t  <mat-select>\r\n\t\t\t\t\t\t    <mat-option value=\"Popularity\">\r\n\t\t\t\t\t\t      Popularity\r\n\t\t\t\t\t\t    </mat-option>\r\n\t\t\t\t\t\t    <mat-option value=\"Top Rated\">\r\n\t\t\t\t\t\t      Top Rated\r\n\t\t\t\t\t\t    </mat-option>\r\n\t\t\t\t\t\t    <mat-option value=\"New\">\r\n\t\t\t\t\t\t      New\r\n\t\t\t\t\t\t    </mat-option>\r\n\t\t\t\t\t\t  </mat-select>\r\n\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-12 col-sm-5 col-md-5 col-lg-4 col-xl-3 d-none d-md-block\">\r\n      \t<div class=\"collection-filter\">\r\n      \t\t<div class=\"collection-filter-block\">\r\n      \t\t\t<mat-accordion multi=\"true\" >\r\n\t\t\t      \t<mat-expansion-panel [expanded]=\"true\">\r\n\t\t\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t\t      Categories\r\n\t\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t\t\t<div class=\"collection-brand-filter\">\r\n\t\t\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Lipstick<label>(786)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Perfume and Body Mist<label>(346)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Nail Polish<label>(811)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Foundation and Primer<label>(178)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Kajal and Eye Liner<label>(92)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Eyeshadow<label>(76)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Face Moisturisers<label>(109)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Shampoo and Conditioner<label>(67)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Make-up Brushes<label>(43)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<p class=\"more\"><a href=\"#\">+ 9 more</a></p>\r\n\t\t\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</mat-expansion-panel>\r\n\t\t\t\t\t\t\r\n\t\t\t      \t<mat-expansion-panel [expanded]=\"true\">\r\n\t\t\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t\t      Brands\r\n\t\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t\t\t<div class=\"collection-brand-filter\">\r\n\t\t\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">M.A.C<label>(786)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Colorbar<label>(346)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Lakme<label>(811)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Revlon<label>(178)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Maybelline<label>(92)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">LOreal<label>(76)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Lotus Herbals<label>(109)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Wet n Wild<label>(67)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">Biotique<label>(43)</label></mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<p class=\"more\"><a href=\"#\">+ 112 more</a></p>\r\n\t\t\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</mat-expansion-panel>\r\n\t\t\t\t\t\r\n\t\t\t      \t<mat-expansion-panel [expanded]=\"true\">\r\n\t\t\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t\t      Discount\r\n\t\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t\t\t<div class=\"collection-brand-filter\">\r\n\t\t\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">50% or more</mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">40% or more</mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">30% or more</mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">20% or more</mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">10% or more</mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox aria-label=\"\">10% or below</mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</mat-expansion-panel>\r\n\t\t\t\t\t\t</mat-accordion>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n      </div>\r\n      <div class=\"col-12 col-sm-12 col-md-7 col-lg-8 col-xl-9\">\r\n      \t<div class=\"row\">\r\n\t\t      <div class=\"col-6 col-sm-6 col-md-6 col-lg-4 col-xl-4 p-xs-0\" *ngFor=\"let product of products\">\r\n\t\t      \t<div class=\"item-box\">\r\n\t\t          <div class=\"img-box\">\r\n\t\t            <img [src]=\"product.img\" alt=\"\" class=\"img-fluid mx-auto d-block\">\r\n\t\t            <div class=\"action tl\">\r\n\t\t              <div class=\"action-item\">\r\n\t\t                <a href=\"\"><img src=\"./assets/img/i-like.png\"></a>\r\n\t\t              </div>\r\n\t\t               <div class=\"action-item\">\r\n\t\t                 <a href=\"\"><img src=\"./assets/img/i-full-size.png\"></a>\r\n\t\t               </div>\r\n\t\t            </div>\r\n\t\t            <div class=\"action bc\">\r\n\t\t              <button type=\"button\" class=\"btn btn-pri\" routerLink=\"/product-details\" routerLinkActive=\"active\">Shop Now</button>\r\n\t\t            </div>\r\n\t\t          </div>\r\n\t\t          <div class=\"info-box text-center\">\r\n\t\t          \t<h2>{{product.brand}}</h2>\r\n\t\t            <h1>{{product.name}}</h1>\r\n\t\t            <p>{{product.price}}</p>\r\n\t\t          </div>\r\n\t\t        </div>\r\n\t\t      </div>\r\n\t\t    </div>\r\n\t\t    <div class=\"row\">\r\n\t\t    \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t\t    \t\t<ul class=\"pagination pagination-sm justify-content-end\">\r\n\t\t    \t\t\t<span class=\"d-none d-md-block\">1 of 14 pages</span>\r\n\t\t\t\t\t\t  <li class=\"page-item \"><a class=\"page-link border-box\" href=\"#\">Prev</a></li>\r\n\t\t\t\t\t\t  <li class=\"page-item\"><a class=\"page-link\" href=\"#\">1</a></li>\r\n\t\t\t\t\t\t  <li class=\"page-item active\"><a class=\"page-link\" href=\"#\">2</a></li>\r\n\t\t\t\t\t\t  <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\r\n\t\t\t\t\t\t  <li class=\"page-item\"><a class=\"page-link\" href=\"#\">4</a></li>\r\n\t\t\t\t\t\t  <li class=\"page-item\"><a class=\"page-link\" href=\"#\">5</a></li>\r\n\t\t\t\t\t\t  <li class=\"page-item\"><a class=\"page-link border-box\" href=\"#\">Next</a></li>\r\n\t\t\t\t\t\t</ul>\r\n\t\t    \t</div>\r\n\t\t    </div>\r\n\t\t  </div>\r\n\t\t</div>\r\n\t</div>\r\n</section>\r\n<section class=\"d-block d-md-none bottom-filter\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row \">\r\n\t\t\t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t\t  \t<mat-card>\r\n\t\t  \t\t<div class=\"row\">\r\n\t\t  \t\t\t<div class=\"col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6\">\r\n\t\t  \t\t\t\t<div class=\"text-center\">\r\n\t\t  \t\t\t\t\t<button mat-button (click)=\"openBottomSheet()\"><i class=\"fa fa-filter\" aria-hidden=\"true\"></i> Filter</button>\r\n\t\t  \t\t\t\t</div>\r\n\t\t  \t\t\t</div>\r\n\t\t  \t\t\t<div class=\"col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6\">\r\n\t\t  \t\t\t\t<div class=\"text-center\">\r\n\t\t  \t\t\t\t\t<button mat-button (click)=\"openDialog()\"><i class=\"fa fa-text-height\" aria-hidden=\"true\"></i> Sort</button>\r\n\t\t  \t\t\t\t</div>\r\n\t\t  \t\t\t</div>\r\n\t\t  \t\t</div>\r\n\t\t  \t</mat-card>\r\n\t\t  </div>\r\n\t\t</div>\r\n\t</div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/product/product-listing/product-listing.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/product/product-listing/product-listing.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".collection-filter .collection-filter-block ul {\n  padding: 0;\n  list-style: none; }\n  .collection-filter .collection-filter-block ul .more a {\n    font-size: 12px;\n    font-weight: 700;\n    color: #cfa092;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .collection-filter .collection-filter-block .mat-expansion-panel {\n  background-color: #f5f5f5; }\n  .item-box {\n  position: relative;\n  box-shadow: 0 1px 2px 0 rgba(110, 110, 110, 0.302), 0 1px 3px 1px rgba(255, 255, 255, 0.149);\n  margin-bottom: 15px; }\n  .item-box:hover .img-box {\n    border: 1px solid #ddd; }\n  .item-box:hover .img-box .action {\n    display: block; }\n  .item-box .img-box {\n    position: relative;\n    min-height: 350px; }\n  @media (min-width: 768px) and (max-width: 991px) {\n      .item-box .img-box {\n        min-height: 250px; } }\n  @media (max-width: 767px) {\n      .item-box .img-box {\n        min-height: 250px; } }\n  .item-box .img-box .action {\n      display: none;\n      position: absolute; }\n  .item-box .img-box .action .action-item {\n        border-width: 1px;\n        border-color: #dddddd;\n        border-style: solid;\n        background-color: white;\n        padding: 12px; }\n  .item-box .img-box .action .btn-pri {\n        background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n        background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n        box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n        font-size: 14px;\n        color: #000;\n        font-weight: 700;\n        padding: 16px 40px;\n        font-family: \"Libre Baskerville\", serif;\n        border-radius: 0;\n        border-top: 2px solid transparent; }\n  @media (min-width: 768px) and (max-width: 991px) {\n          .item-box .img-box .action .btn-pri {\n            padding: 12px 20px; } }\n  @media (max-width: 767px) {\n          .item-box .img-box .action .btn-pri {\n            padding: 16px 22px; } }\n  .item-box .img-box .action .btn-pri:hover {\n        box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.6);\n        border-top: 2px solid #c6755d;\n        color: #000 !important; }\n  .item-box .img-box .tl {\n      right: 0;\n      top: 0; }\n  .item-box .img-box .bc {\n      bottom: 20px;\n      left: 0;\n      right: 0;\n      text-align: center; }\n  .item-box .info-box h2 {\n    font-size: 14px;\n    font-weight: 700;\n    font-family: \"Libre Franklin\", sans-serif;\n    color: #000;\n    margin: 10px 0 10px; }\n  .item-box .info-box h1 {\n    font-size: 14px;\n    font-weight: 400;\n    font-family: \"Libre Franklin\", sans-serif;\n    color: #000;\n    margin: 15px 0; }\n  .item-box .info-box h1:hover {\n      color: #cfa092; }\n  .item-box .info-box p {\n    font-size: 16px;\n    font-weight: 600;\n    font-family: \"Libre Franklin\", sans-serif;\n    color: #000;\n    padding-bottom: 8px;\n    margin: 0; }\n  .item-box .info-box p del {\n      font-size: 16px;\n      font-weight: 400;\n      font-family: \"Libre Franklin\", sans-serif;\n      color: #000;\n      padding-right: 10px; }\n  .breadcrumb {\n  background-color: transparent;\n  margin-top: 0;\n  margin-bottom: 0;\n  padding: 0; }\n  .breadcrumb a {\n    font-size: 14px;\n    font-weight: 400;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .breadcrumb .active {\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .bottom-filter {\n  position: fixed;\n  bottom: 20px;\n  width: 100%;\n  z-index: 8; }\n  .bottom-filter mat-card {\n    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.5);\n    background-color: #fff;\n    padding: 12px; }\n  .bottom-filter mat-card button {\n      font-size: 16px;\n      font-weight: 500;\n      color: #000;\n      font-family: \"Libre Franklin\", sans-serif; }\n  .pagination {\n  margin: 20px 0; }\n  .pagination span {\n    font-size: 12px;\n    font-weight: 400;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif;\n    line-height: 28px;\n    padding-right: 14px; }\n  .pagination .page-link {\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif;\n    padding: 4px 12px;\n    border: 0; }\n  .pagination .border-box {\n    border: 1px solid #cfa092;\n    z-index: 1;\n    color: #cfa092;\n    border-radius: 0; }\n  .pagination .active .page-link {\n    color: #fff !important;\n    background-color: #cfa092;\n    border-color: #cfa092; }\n  @media (max-width: 767px) {\n  .p-xs-0 {\n    padding: 0; } }\n"

/***/ }),

/***/ "./src/app/product/product-listing/product-listing.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/product/product-listing/product-listing.component.ts ***!
  \**********************************************************************/
/*! exports provided: ProductListingComponent, ProductListingDialogComponent, ProductListingSortDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductListingComponent", function() { return ProductListingComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductListingDialogComponent", function() { return ProductListingDialogComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductListingSortDialogComponent", function() { return ProductListingSortDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/bottom-sheet */ "./node_modules/@angular/material/esm5/bottom-sheet.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var ProductListingComponent = /** @class */ (function () {
    function ProductListingComponent(_bottomSheet, dialog) {
        this._bottomSheet = _bottomSheet;
        this.dialog = dialog;
        this.products = [
            {
                img: './assets/img/na4.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na5.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na6.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na7.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na8.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na9.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na10.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na11.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na14.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na13.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na12.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na15.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na16.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na17.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na18.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na19.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na20.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            },
            {
                img: './assets/img/na6.png',
                brand: 'Maybelline',
                name: 'Unisex Skin Brightening Serum',
                price: '$49'
            }
        ];
        this.panelOpenState = false;
    }
    ProductListingComponent.prototype.openBottomSheet = function () {
        this._bottomSheet.open(ProductListingDialogComponent);
    };
    //sort
    ProductListingComponent.prototype.openDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(ProductListingSortDialogComponent, {
            width: '350px',
            height: '200px',
            data: { name: this.name, animal: this.animal }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed');
            _this.animal = result;
        });
    };
    ProductListingComponent.prototype.ngOnInit = function () {
        //this.teams = this.route.snapshot.data['teams'];
    };
    ProductListingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-listing',
            template: __webpack_require__(/*! ./product-listing.component.html */ "./src/app/product/product-listing/product-listing.component.html"),
            styles: [__webpack_require__(/*! ./product-listing.component.scss */ "./src/app/product/product-listing/product-listing.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_2__["MatBottomSheet"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], ProductListingComponent);
    return ProductListingComponent;
}());

var ProductListingDialogComponent = /** @class */ (function () {
    function ProductListingDialogComponent(_bottomSheetRef) {
        this._bottomSheetRef = _bottomSheetRef;
    }
    ProductListingDialogComponent.prototype.openLink = function (event) {
        this._bottomSheetRef.dismiss();
        event.preventDefault();
    };
    ProductListingDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-listing-dialog',
            template: __webpack_require__(/*! ./product-listing-dialog.component.html */ "./src/app/product/product-listing/product-listing-dialog.component.html"),
            styles: [__webpack_require__(/*! ./product-listing.component.scss */ "./src/app/product/product-listing/product-listing.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_2__["MatBottomSheetRef"]])
    ], ProductListingDialogComponent);
    return ProductListingDialogComponent;
}());

var ProductListingSortDialogComponent = /** @class */ (function () {
    function ProductListingSortDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    ProductListingSortDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    ProductListingSortDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-listing-sort-dialog',
            template: __webpack_require__(/*! ./product-listing-sort-dialog.component.html */ "./src/app/product/product-listing/product-listing-sort-dialog.component.html"),
            styles: [__webpack_require__(/*! ./product-listing.component.scss */ "./src/app/product/product-listing/product-listing.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], ProductListingSortDialogComponent);
    return ProductListingSortDialogComponent;
}());



/***/ }),

/***/ "./src/app/product/wishlist/wishlist.component.html":
/*!**********************************************************!*\
  !*** ./src/app/product/wishlist/wishlist.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  wishlist works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/product/wishlist/wishlist.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/product/wishlist/wishlist.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/product/wishlist/wishlist.component.ts":
/*!********************************************************!*\
  !*** ./src/app/product/wishlist/wishlist.component.ts ***!
  \********************************************************/
/*! exports provided: WishlistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WishlistComponent", function() { return WishlistComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WishlistComponent = /** @class */ (function () {
    function WishlistComponent() {
    }
    WishlistComponent.prototype.ngOnInit = function () {
    };
    WishlistComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-wishlist',
            template: __webpack_require__(/*! ./wishlist.component.html */ "./src/app/product/wishlist/wishlist.component.html"),
            styles: [__webpack_require__(/*! ./wishlist.component.scss */ "./src/app/product/wishlist/wishlist.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], WishlistComponent);
    return WishlistComponent;
}());



/***/ }),

/***/ "./src/app/search/search.component.html":
/*!**********************************************!*\
  !*** ./src/app/search/search.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"search search-main\">\r\n\t<button type=\"button\" class=\"btn btn-close\" (click)=\"closeSearch($event)\"><img src=\"./assets/img/close.svg\" alt=\"Close\"></button>\r\n\t<div class=\"search__inner search__inner--up\">\r\n\t\t<form class=\"search__form\">\r\n\t\t\t<input class=\"search__input\" name=\"s\" type=\"search\" placeholder=\"What are you looking for?\" autocomplete=\"off\" spellcheck=\"false\">\r\n\t\t\t<span class=\"search__info\">Hit enter to search</span>\r\n\t\t</form>\r\n\t</div>\r\n\t<div class=\"search__inner search__inner--down\">\r\n\t\t<div class=\"search__related\"></div>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "./src/app/search/search.component.scss":
/*!**********************************************!*\
  !*** ./src/app/search/search.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".search {\n  opacity: 0; }\n  .search .btn-close {\n    position: absolute;\n    right: 15px;\n    top: 15px;\n    z-index: 1; }\n  .search .btn-close img {\n      width: 32px; }\n  @media (max-width: 480px) {\n        .search .btn-close img {\n          width: 22px; } }\n  .search.search--open {\n    position: fixed;\n    z-index: 1000;\n    top: 0;\n    left: 0;\n    overflow: hidden;\n    width: 100%;\n    height: 100vh;\n    text-align: center;\n    opacity: 1; }\n  .search.search--open .search__form {\n      width: 75%;\n      max-width: 900px; }\n  .search.search--open .search__form .search__input {\n        border: 0;\n        background: transparent;\n        border-radius: 0;\n        -webkit-appearance: none;\n        font-size: 52px;\n        line-height: 1;\n        display: inline-block;\n        box-sizing: border-box;\n        width: 100%;\n        padding: 0 0 0.1em 0;\n        outline: none;\n        border-bottom: 4px solid !important; }\n  @media (min-width: 768px) and (max-width: 991px) {\n          .search.search--open .search__form .search__input {\n            font-size: 42px; } }\n  @media (max-width: 767px) {\n          .search.search--open .search__form .search__input {\n            font-size: 32px; } }\n  @media (max-width: 480px) {\n          .search.search--open .search__form .search__input {\n            font-size: 18px; } }\n  .search.search--open .search__form .search__info {\n        font-size: 90%;\n        font-weight: bold;\n        display: block;\n        width: 100%;\n        margin: 0 auto;\n        padding: 0.85em 0;\n        text-align: right; }\n  .search.search--open .search__inner {\n      -webkit-transform: translate3d(0, 0, 0);\n              transform: translate3d(0, 0, 0); }\n  .search.search--open .search__inner--down {\n      top: 50%; }\n  .search .search__inner {\n    transition: -webkit-transform 0.6s;\n    transition: transform 0.6s;\n    transition: transform 0.6s, -webkit-transform 0.6s;\n    transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);\n    position: absolute;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: 100%;\n    height: 50%;\n    background: #ffffff; }\n  .search .search__inner--down {\n    -webkit-transform: translate3d(0, 100%, 0);\n            transform: translate3d(0, 100%, 0);\n    color: #f8f8f8;\n    background: #f8f8f8; }\n  .search .search__inner--up {\n    -webkit-transform: translate3d(0, -100%, 0);\n            transform: translate3d(0, -100%, 0);\n    background: #ffffff; }\n"

/***/ }),

/***/ "./src/app/search/search.component.ts":
/*!********************************************!*\
  !*** ./src/app/search/search.component.ts ***!
  \********************************************/
/*! exports provided: SearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SearchComponent = /** @class */ (function () {
    function SearchComponent() {
    }
    SearchComponent.prototype.ngOnInit = function () {
    };
    SearchComponent.prototype.closeSearch = function ($event) {
        var searchEle = document.getElementsByClassName('search-main');
        if (searchEle[0].classList.contains("search--open")) {
            searchEle[0].classList.remove("search--open");
        }
        else {
            searchEle[0].classList.add("search--open");
        }
    };
    SearchComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-search',
            template: __webpack_require__(/*! ./search.component.html */ "./src/app/search/search.component.html"),
            styles: [__webpack_require__(/*! ./search.component.scss */ "./src/app/search/search.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SearchComponent);
    return SearchComponent;
}());



/***/ }),

/***/ "./src/app/static-pages/about/about.component.html":
/*!*********************************************************!*\
  !*** ./src/app/static-pages/about/about.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"page-wrap mb-5\">\r\n   <div class=\"container\">\r\n\t    <div class=\"row\">\r\n\t      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t<ul class=\"breadcrumb\">\r\n\t\t\t  \t<li class=\"breadcrumb-item\"><a routerLink=\"/\">Home</a></li>\r\n\t\t\t  \t<li class=\"breadcrumb-item active\">About Us</li>\r\n\t\t\t</ul>\r\n\t      </div>\r\n\t    </div>\r\n\t    <div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t\t<div class=\"about-bg\">\r\n\t      \t\t\t<h1 class=\"page-head text-center mb-3\">About Us</h1>\r\n\t      \t\t</div>\r\n\t      \t</div>\r\n\t    </div>\r\n\t    <div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t\t<div class=\"about-info text-center\">\r\n\t      \t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t      \t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t      \t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n    </div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/static-pages/about/about.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/static-pages/about/about.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".breadcrumb {\n  background-color: transparent;\n  margin-top: 0;\n  margin-bottom: 0;\n  padding: 0; }\n  .breadcrumb a {\n    font-size: 14px;\n    font-weight: 400;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .breadcrumb .active {\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .about-bg {\n  background-image: url('about-bg.png');\n  background-repeat: no-repeat;\n  background-position: bottom;\n  background-size: cover;\n  padding: 90px 0;\n  position: relative;\n  margin: 20px 0; }\n  @media (max-width: 767px) {\n    .about-bg {\n      padding: 40px 0; } }\n  .about-info p {\n  font-size: 16px;\n  font-weight: 400;\n  color: #000;\n  font-family: \"Libre Franklin\", sans-serif;\n  line-height: 28px;\n  letter-spacing: .5px; }\n  @media (max-width: 767px) {\n    .about-info p {\n      font-size: 14px;\n      line-height: 24px; } }\n"

/***/ }),

/***/ "./src/app/static-pages/about/about.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/static-pages/about/about.component.ts ***!
  \*******************************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutComponent = /** @class */ (function () {
    function AboutComponent() {
    }
    AboutComponent.prototype.ngOnInit = function () {
    };
    AboutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-about',
            template: __webpack_require__(/*! ./about.component.html */ "./src/app/static-pages/about/about.component.html"),
            styles: [__webpack_require__(/*! ./about.component.scss */ "./src/app/static-pages/about/about.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "./src/app/static-pages/contact-form/contact-form.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/static-pages/contact-form/contact-form.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"user-input-wrap\">\r\n  \t<form class=\"example-form\" [formGroup]=\"userValidations\" (ngSubmit)=\"userValidations.valid && sendMessageToParent()\" novalidate>\r\n\t\t<mat-form-field class=\"example-full-width  d-block\">\r\n\t\t\t<mat-label>Your Name</mat-label>\r\n\t\t\t<input matInput formControlName=\"firstName\" placeholder=\"Your Name\" required>\r\n\t\t\t\r\n\t\t    <mat-error *ngIf=\"userValidations.get('firstName').hasError('required')\">\r\n\t\t      \tName is Required!\r\n\t\t    </mat-error>\r\n\t\t    <mat-error *ngIf=\"userValidations.get('firstName').hasError('pattern')\">\r\n\t\t      \tName must follow this pattern!\r\n\t\t    </mat-error>\r\n\t\t</mat-form-field>\r\n\t\t<mat-form-field class=\"example-full-width d-block\">\r\n\t\t\t<mat-label>Your E-mail</mat-label>\r\n\t\t\t<input matInput formControlName=\"email\" placeholder=\"Your E-mail\" required>\r\n\t\t\t<mat-error *ngIf=\"userValidations.get('email').hasError('required')\">\r\n\t\t      \tE-mail is Required!\r\n\t\t    </mat-error>\r\n\t\t    <mat-error *ngIf=\"userValidations.get('email').hasError('pattern')\">\r\n\t\t      \tPlease enter a valid email address\r\n\t\t    </mat-error>\r\n\t\t</mat-form-field>\r\n\t\t<mat-form-field class=\"d-block\">\r\n\t\t   \t<mat-label>Select Your {{receivedParentMessage}} query from the list</mat-label>\r\n\t\t    <mat-select formControlName=\"query\" required>\r\n\t\t\t    <mat-option>Select</mat-option>\r\n\t\t\t    <mat-option *ngFor=\"let query of querys\" [value]=\"query\">\r\n\t\t\t      {{query.name}}\r\n\t\t\t    </mat-option>\r\n\t\t  \t</mat-select>\r\n\t\t  \t<mat-error *ngIf=\"userValidations.get('query').hasError('required')\">\r\n\t\t      \tPlease choose an your {{receivedParentMessage}} query\r\n\t\t    </mat-error>\r\n\t\t</mat-form-field>\r\n\t\t<mat-form-field class=\"example-full-width d-block\">\r\n\t\t\t<mat-label>Leave a comment</mat-label>\r\n\t\t\t<textarea matInput formControlName=\"comment\" placeholder=\"Ex. It makes me feel...\" required></textarea>\r\n\t\t\t<mat-error *ngIf=\"userValidations.get('comment').hasError('required')\">\r\n\t\t      Comment is <strong>required</strong>\r\n\t\t    </mat-error>\r\n\t\t</mat-form-field>\r\n\t\t<div class=\"form-group text-center mt-3\">\r\n\t    \t<button  type=\"submit\" class=\"btn btn-po\">Submit</button>\r\n\t    </div>\r\n\t</form>\r\n\t\r\n</div>"

/***/ }),

/***/ "./src/app/static-pages/contact-form/contact-form.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/static-pages/contact-form/contact-form.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".user-input-wrap {\n  box-shadow: 0 1px 2px 0 rgba(60, 64, 67, 0.302), 0 1px 3px 1px rgba(60, 64, 67, 0.149);\n  transition: box-shadow 0.08s linear, min-width 0.15s cubic-bezier(0.4, 0, 0.2, 1);\n  padding: 15px;\n  margin: 20px 0 40px; }\n  .user-input-wrap .btn-po {\n    background-image: linear-gradient(to right, #cfa092, #f0e4e0, #d8bcb3);\n    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);\n    margin-right: 20px;\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Baskerville\", serif;\n    padding: 14px 40px;\n    border-radius: 0;\n    border-top: 2px solid transparent; }\n  .user-input-wrap .btn-po:hover, .user-input-wrap .btn-po:focus {\n      box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.6);\n      border-top: 2px solid #c6755d; }\n"

/***/ }),

/***/ "./src/app/static-pages/contact-form/contact-form.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/static-pages/contact-form/contact-form.component.ts ***!
  \*********************************************************************/
/*! exports provided: ContactFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactFormComponent", function() { return ContactFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactFormComponent = /** @class */ (function () {
    function ContactFormComponent(formBuilder) {
        this.formBuilder = formBuilder;
        this.messageToEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.querys = [
            { name: 'How do I return my product' },
            { name: 'How will I get my refund' },
            { name: 'I have forgotten my password' },
            { name: 'I want to change my email address.' },
            { name: 'How do I check the status of my order?' }
        ];
    }
    ContactFormComponent.prototype.ngOnInit = function () {
        this.userValidations = this.formBuilder.group({
            firstName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[a-zA-Z]+')]],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            query: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            comment: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]]
        });
    };
    ContactFormComponent.prototype.sendMessageToParent = function () {
        this.userValidations.value.contactType = this.receivedParentMessage;
        this.messageToEmit.emit(this.userValidations);
        //this.userValidations.reset();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], ContactFormComponent.prototype, "receivedParentMessage", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ContactFormComponent.prototype, "messageToEmit", void 0);
    ContactFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contact-form',
            template: __webpack_require__(/*! ./contact-form.component.html */ "./src/app/static-pages/contact-form/contact-form.component.html"),
            styles: [__webpack_require__(/*! ./contact-form.component.scss */ "./src/app/static-pages/contact-form/contact-form.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], ContactFormComponent);
    return ContactFormComponent;
}());



/***/ }),

/***/ "./src/app/static-pages/contact-us/contact-us.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/static-pages/contact-us/contact-us.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"page-wrap mb-5 contact-page\">\r\n    <div class=\"container\">\r\n\t    <div class=\"row\">\r\n\t      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t<ul class=\"breadcrumb\">\r\n\t\t\t  \t<li class=\"breadcrumb-item\"><a routerLink=\"/\">Home</a></li>\r\n\t\t\t  \t<li class=\"breadcrumb-item active\">Contact Us</li>\r\n\t\t\t</ul>\r\n\t      </div>\r\n\t    </div>\r\n\t    <div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t\t<h1 class=\"page-head text-center mb-3\">How can we help you?</h1>\r\n\t      \t</div>\r\n\t    </div>\r\n\t    <div class=\"big-devices d-none d-sm-block\" >\r\n\t    \t<div class=\"row mt-3\">\r\n\t\t      \t<div class=\"col-12 col-sm-4 col-md-4 offset-lg-1 col-lg-3 offset-xl-1 col-xl-3\">\r\n\t\t      \t\t<p class=\"type-head\">Select Query Type</p>\r\n\t\t\t\t\t<ul class=\"nav flex-column nav-tabs\">\r\n\t\t\t\t\t  <li class=\"nav-item\" (click)=\"yourFn($event)\">\r\n\t\t\t\t\t    <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#Delivery\">Delivery</a>\r\n\t\t\t\t\t  </li>\r\n\t\t\t\t\t  <li class=\"nav-item\" (click)=\"yourFn($event)\">\r\n\t\t\t\t\t    <a class=\"nav-link\" data-toggle=\"tab\" href=\"#Return\">Return</a>\r\n\t\t\t\t\t  </li>\r\n\t\t\t\t\t  <li class=\"nav-item\" (click)=\"yourFn($event)\">\r\n\t\t\t\t\t    <a class=\"nav-link\" data-toggle=\"tab\" href=\"#Refund\">Refund</a>\r\n\t\t\t\t\t  </li>\r\n\t\t\t\t\t  <li class=\"nav-item\" (click)=\"yourFn($event)\">\r\n\t\t\t\t\t    <a class=\"nav-link\" data-toggle=\"tab\" href=\"#Account\">Manage Account</a>\r\n\t\t\t\t\t  </li>\r\n\t\t\t\t\t  <li class=\"nav-item\" (click)=\"yourFn($event)\">\r\n\t\t\t\t\t    <a class=\"nav-link\" data-toggle=\"tab\" href=\"#General\">General Enqueries</a>\r\n\t\t\t\t\t  </li>\r\n\t\t\t\t\t</ul>\r\n\t            </div>\r\n\t            <div class=\"col-12 col-sm-8  offset-md-1 col-md-7  offset-lg-1 col-lg-6 offset-xl-1 col-xl-6\">\r\n\t\t\t\t\t<div class=\"tab-content\">\r\n\t\t\t\t\t    <div class=\"tab-pane active\" id=\"Delivery\">\r\n\t\t\t\t\t    \t<app-contact-form [receivedParentMessage]=\"messageToSendParent\" (messageToEmit)=\"getMessage($event)\"></app-contact-form>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"tab-pane\" id=\"Return\">\r\n\t\t\t\t\t\t\t<app-contact-form [receivedParentMessage]=\"messageToSendParent\" (messageToEmit)=\"getMessage($event)\"></app-contact-form>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"tab-pane\" id=\"Refund\">\r\n\t\t\t\t\t\t\t<app-contact-form [receivedParentMessage]=\"messageToSendParent\" (messageToEmit)=\"getMessage($event)\"></app-contact-form>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"tab-pane\" id=\"Account\">\r\n\t\t\t\t\t\t\t<app-contact-form [receivedParentMessage]=\"messageToSendParent\" (messageToEmit)=\"getMessage($event)\"></app-contact-form>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"tab-pane\" id=\"General\">\r\n\t\t\t\t\t\t\t<app-contact-form [receivedParentMessage]=\"messageToSendParent\" (messageToEmit)=\"getMessage($event)\"></app-contact-form>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t    </div>\r\n\t    </div>\r\n\t   \r\n\t\t<div class=\"small-devices d-block d-sm-none\">\r\n\t\t\t<div class=\"row mt-3\">\r\n\t\t      \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t\t      \t\t<mat-tab-group (selectedTabChange)=\"yourFn($event)\">\r\n\t\t\t\t\t\t<mat-tab label=\"Delivery\">\r\n\t\t\t\t\t\t\t<app-contact-form [receivedParentMessage]=\"messageToSendParent\" (messageToEmit)=\"getMessage($event)\"></app-contact-form>\r\n\t\t\t\t\t\t</mat-tab>\r\n\t\t\t\t\t\t<mat-tab label=\"Return\">\r\n\t\t\t\t\t\t\t<app-contact-form [receivedParentMessage]=\"messageToSendParent\" (messageToEmit)=\"getMessage($event)\"></app-contact-form>\r\n\t\t\t\t\t\t</mat-tab>\r\n\t\t\t\t\t\t<mat-tab label=\"Refund\">\r\n\t\t\t\t\t\t\t<app-contact-form [receivedParentMessage]=\"messageToSendParent\" (messageToEmit)=\"getMessage($event)\"></app-contact-form>\r\n\t\t\t\t\t\t</mat-tab>\r\n\t\t\t\t\t\t<mat-tab label=\"Manage Account\">\r\n\t\t\t\t\t\t\t<app-contact-form [receivedParentMessage]=\"messageToSendParent\" (messageToEmit)=\"getMessage($event)\"></app-contact-form>\r\n\t\t\t\t\t\t</mat-tab>\r\n\t\t\t\t\t\t<mat-tab label=\"General Enqueries\">\r\n\t\t\t\t\t\t\t<app-contact-form [receivedParentMessage]=\"messageToSendParent\" (messageToEmit)=\"getMessage($event)\"></app-contact-form>\r\n\t\t\t\t\t\t</mat-tab>\r\n\t\t\t\t\t</mat-tab-group>\r\n\t\t        </div>\r\n\t\t    </div>\r\n\t\t</div>\r\n\t\t<div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t  \t<div class=\"contact-call\">\r\n\t      \t  \t\t<p> \r\n\t      \t  \t\t\t<i class=\"fa fa-phone\" aria-hidden=\"true\"></i>\r\n      \t  \t\t\t\t<span class=\"call-head\">Call Us at</span>\r\n  \t  \t\t\t\t\t<span class=\"number\">9960627691</span>\r\n\t      \t  \t\t\t<span class=\"info d-none d-sm-block\">Call from your registered number for a better experience.</span>\r\n\t      \t  \t\t</p>\r\n\t      \t  \t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"row mb-5 mt-5\">\r\n\t      \t<div class=\"col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 align-self-center\">\r\n\t      \t\t<div class=\"address-info\">\r\n\t      \t\t\t<h3>Corporate Office</h3>\r\n\t      \t\t\t<p>Mahi Softwares Pvt Ltd</p>\r\n\t      \t\t\t<p>414501 Renukaiwadi,</p>\r\n\t      \t\t\t<p>Ahmednagar, </p>\r\n\t      \t\t\t<p>Maharashtra- 414501, India</p>\r\n\t      \t\t</div>\r\n\t      \t</div>\r\n\t      \t<div class=\"col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8\">\r\n               <div class=\"location-map\">\r\n               \t<img src=\"./assets/img/map.png\" alt=\"map\" title=\"location\" class=\"img-fluid\">\r\n               </div>\r\n\t      \t</div>\r\n        </div>\r\n    </div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/static-pages/contact-us/contact-us.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/static-pages/contact-us/contact-us.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".breadcrumb {\n  background-color: transparent;\n  margin-top: 0;\n  margin-bottom: 0;\n  padding: 0; }\n  .breadcrumb a {\n    font-size: 14px;\n    font-weight: 400;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .breadcrumb .active {\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .contact-page .type-head {\n  font-size: 14px;\n  font-weight: 700;\n  color: #000;\n  font-family: \"Libre Baskerville\", serif; }\n  .contact-page .nav-tabs .nav-link {\n  font-size: 14px;\n  font-weight: 400;\n  color: #000;\n  font-family: \"Libre Franklin\", sans-serif;\n  padding: 16px 12px; }\n  .contact-page .nav-tabs .nav-link.active, .contact-page .nav-tabs .nav-link:focus, .contact-page .nav-tabs .nav-link:hover {\n    font-weight: 700;\n    border: none;\n    border-right: 4px solid #cfa092;\n    border-radius: 0; }\n  .contact-page .nav-tabs .nav-item {\n  border-bottom: 1px solid #ddd; }\n  .contact-page .contact-call {\n  border-width: 1px;\n  border-color: #cfa092;\n  border-style: dashed;\n  padding: 15px 20px;\n  background-color: #f4eae8;\n  margin-bottom: 16px; }\n  .contact-page .contact-call p {\n    margin: 0;\n    font-size: 12px;\n    font-weight: 400;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .contact-page .contact-call .fa {\n    font-size: 16px; }\n  .contact-page .contact-call .call-head {\n    padding-left: 14px;\n    padding-right: 22px; }\n  .contact-page .contact-call .number {\n    font-size: 24px;\n    font-weight: 400;\n    color: #000;\n    font-family: \"Libre Baskerville\", serif; }\n  @media (max-width: 767px) {\n      .contact-page .contact-call .number {\n        font-size: 20px; } }\n  .contact-page .contact-call .info {\n    float: right;\n    padding-top: 12px; }\n  .contact-page .address-info h3 {\n  font-weight: 600;\n  font-size: 16px;\n  color: #000;\n  font-family: \"Libre Baskerville\", serif; }\n  .contact-page .address-info p {\n  font-size: 14px;\n  font-weight: 400;\n  color: #000;\n  margin: 0;\n  font-family: \"Libre Franklin\", sans-serif; }\n  .contact-page .location-map {\n  margin: 10px 0; }\n"

/***/ }),

/***/ "./src/app/static-pages/contact-us/contact-us.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/static-pages/contact-us/contact-us.component.ts ***!
  \*****************************************************************/
/*! exports provided: ContactUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsComponent", function() { return ContactUsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/esm5/snack-bar.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactUsComponent = /** @class */ (function () {
    function ContactUsComponent(_snackBar) {
        this._snackBar = _snackBar;
        this.messageToSendParent = 'Delivery';
    }
    ContactUsComponent.prototype.ngOnInit = function () {
    };
    ContactUsComponent.prototype.getMessage = function (message) {
        this.receivedChildData = message;
        this._snackBar.open("Request Submitted Successfully...!", 'Thanks', {
            duration: 2000,
            verticalPosition: 'top',
            horizontalPosition: 'end'
        });
        console.log(this.receivedChildData);
    };
    ContactUsComponent.prototype.yourFn = function ($event) {
        this.messageToSendParent = $event.tab ? $event.tab.textLabel : $event.target.text;
    };
    ContactUsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contact-us',
            template: __webpack_require__(/*! ./contact-us.component.html */ "./src/app/static-pages/contact-us/contact-us.component.html"),
            styles: [__webpack_require__(/*! ./contact-us.component.scss */ "./src/app/static-pages/contact-us/contact-us.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"]])
    ], ContactUsComponent);
    return ContactUsComponent;
}());



/***/ }),

/***/ "./src/app/static-pages/faq/faq.component.html":
/*!*****************************************************!*\
  !*** ./src/app/static-pages/faq/faq.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"page-wrap mb-5\">\r\n   <div class=\"container\">\r\n\t    <div class=\"row\">\r\n\t      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t<ul class=\"breadcrumb\">\r\n\t\t\t  \t<li class=\"breadcrumb-item\"><a routerLink=\"/\">Home</a></li>\r\n\t\t\t  \t<li class=\"breadcrumb-item active\">FAQ</li>\r\n\t\t\t</ul>\r\n\t      </div>\r\n\t    </div>\r\n\t    <div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t\t<h1 class=\"page-head text-center mb-3\">Frequently Asked Questions</h1>\r\n\t      \t</div>\r\n\t    </div>\r\n\t    <div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t\t\t\t<mat-accordion>\r\n\t\t\t\t  \t<mat-expansion-panel [expanded]=\"true\">\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      What is Glo Beauty's Return and Exchange Policy? How does it work?\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\r\n\t\t\t\t\t<mat-expansion-panel>\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      To return a product to Glo Beauty, please follow these steps:\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fuagiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\r\n\t\t\t\t\t<mat-expansion-panel>\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      Why has my return been put on hold despite No Questions Asked Returns Policy?\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fuagiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\r\n\t\t\t\t\t<mat-expansion-panel>\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      How do I return multiple products from a single order?\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fuagiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\r\n\t\t\t\t\t<mat-expansion-panel>\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      Why has my return request been declined?\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fuagiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\r\n\t\t\t\t\t<mat-expansion-panel>\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      How long would it take me to receive the refund of the returned product?\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fuagiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\t\t\t\t</mat-accordion>\r\n\t\t\t</div>\r\n\t\t</div>\r\n    </div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/static-pages/faq/faq.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/static-pages/faq/faq.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".breadcrumb {\n  background-color: transparent;\n  margin-top: 0;\n  margin-bottom: 0;\n  padding: 0; }\n  .breadcrumb a {\n    font-size: 14px;\n    font-weight: 400;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .breadcrumb .active {\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .policy-details p {\n  font-size: 14px;\n  font-weight: 400;\n  color: #000;\n  font-family: \"Libre Franklin\", sans-serif; }\n  .mat-expansion-panel {\n  margin: 20px 0; }\n  @media (max-width: 767px) {\n  .mat-expansion-panel-header {\n    height: auto !important;\n    padding: 10px 24px !important; } }\n"

/***/ }),

/***/ "./src/app/static-pages/faq/faq.component.ts":
/*!***************************************************!*\
  !*** ./src/app/static-pages/faq/faq.component.ts ***!
  \***************************************************/
/*! exports provided: FaqComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaqComponent", function() { return FaqComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FaqComponent = /** @class */ (function () {
    function FaqComponent() {
    }
    FaqComponent.prototype.ngOnInit = function () {
    };
    FaqComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-faq',
            template: __webpack_require__(/*! ./faq.component.html */ "./src/app/static-pages/faq/faq.component.html"),
            styles: [__webpack_require__(/*! ./faq.component.scss */ "./src/app/static-pages/faq/faq.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FaqComponent);
    return FaqComponent;
}());



/***/ }),

/***/ "./src/app/static-pages/return-policy/return-policy.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/static-pages/return-policy/return-policy.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"page-wrap mb-5\">\r\n   <div class=\"container\">\r\n\t    <div class=\"row\">\r\n\t      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t<ul class=\"breadcrumb\">\r\n\t\t\t  \t<li class=\"breadcrumb-item\"><a routerLink=\"/\">Home</a></li>\r\n\t\t\t  \t<li class=\"breadcrumb-item active\">Return Policy</li>\r\n\t\t\t</ul>\r\n\t      </div>\r\n\t    </div>\r\n\t    <div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t\t<h1 class=\"page-head text-center mb-3\">Return Policy</h1>\r\n\t      \t</div>\r\n\t    </div>\r\n\t    <div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t\t\t\t<mat-accordion multi=\"true\" >\r\n\t\t\t\t  \t<mat-expansion-panel [expanded]=\"true\">\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fuagiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\r\n\t\t\t\t\t<mat-expansion-panel>\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      Consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fuagiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\r\n\t\t\t\t\t<mat-expansion-panel>\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      Ut enim ad minima veniam, quis nostrum exercitationem\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fuagiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\r\n\t\t\t\t\t<mat-expansion-panel>\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fuagiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\r\n\t\t\t\t\t<mat-expansion-panel>\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fuagiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\r\n\t\t\t\t\t<mat-expansion-panel>\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t    <mat-panel-title>\r\n\t\t\t\t\t\t      Excepteur sint occaecat cupidatat non proident.\r\n\t\t\t\t\t\t    </mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div class=\"policy-details\">\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fuagiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t\t<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\t\t\t\t</mat-accordion>\r\n\t\t\t</div>\r\n\t\t</div>\r\n    </div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/static-pages/return-policy/return-policy.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/static-pages/return-policy/return-policy.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".breadcrumb {\n  background-color: transparent;\n  margin-top: 0;\n  margin-bottom: 0;\n  padding: 0; }\n  .breadcrumb a {\n    font-size: 14px;\n    font-weight: 400;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .breadcrumb .active {\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .policy-details p {\n  font-size: 14px;\n  font-weight: 400;\n  color: #000;\n  font-family: \"Libre Franklin\", sans-serif; }\n  .mat-expansion-panel {\n  margin: 20px 0; }\n  @media (max-width: 767px) {\n  .mat-expansion-panel-header {\n    height: auto !important;\n    padding: 10px 24px !important; } }\n"

/***/ }),

/***/ "./src/app/static-pages/return-policy/return-policy.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/static-pages/return-policy/return-policy.component.ts ***!
  \***********************************************************************/
/*! exports provided: ReturnPolicyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReturnPolicyComponent", function() { return ReturnPolicyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ReturnPolicyComponent = /** @class */ (function () {
    function ReturnPolicyComponent() {
    }
    ReturnPolicyComponent.prototype.ngOnInit = function () {
    };
    ReturnPolicyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-return-policy',
            template: __webpack_require__(/*! ./return-policy.component.html */ "./src/app/static-pages/return-policy/return-policy.component.html"),
            styles: [__webpack_require__(/*! ./return-policy.component.scss */ "./src/app/static-pages/return-policy/return-policy.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ReturnPolicyComponent);
    return ReturnPolicyComponent;
}());



/***/ }),

/***/ "./src/app/static-pages/static-pages.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/static-pages/static-pages.module.ts ***!
  \*****************************************************/
/*! exports provided: StaticPagesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaticPagesModule", function() { return StaticPagesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _return_policy_return_policy_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./return-policy/return-policy.component */ "./src/app/static-pages/return-policy/return-policy.component.ts");
/* harmony import */ var _faq_faq_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./faq/faq.component */ "./src/app/static-pages/faq/faq.component.ts");
/* harmony import */ var _terms_use_terms_use_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./terms-use/terms-use.component */ "./src/app/static-pages/terms-use/terms-use.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./about/about.component */ "./src/app/static-pages/about/about.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/static-pages/contact-us/contact-us.component.ts");
/* harmony import */ var _contact_form_contact_form_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./contact-form/contact-form.component */ "./src/app/static-pages/contact-form/contact-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/esm5/snack-bar.es5.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _static_routing_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./static-routing.module */ "./src/app/static-pages/static-routing.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var StaticPagesModule = /** @class */ (function () {
    function StaticPagesModule() {
    }
    StaticPagesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_3__["MatExpansionModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_12__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ReactiveFormsModule"],
                _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_13__["MatSnackBarModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_14__["MatTabsModule"],
                _static_routing_module__WEBPACK_IMPORTED_MODULE_15__["StaticRoutingModule"]
            ],
            declarations: [_return_policy_return_policy_component__WEBPACK_IMPORTED_MODULE_6__["ReturnPolicyComponent"], _faq_faq_component__WEBPACK_IMPORTED_MODULE_7__["FaqComponent"], _terms_use_terms_use_component__WEBPACK_IMPORTED_MODULE_8__["TermsUseComponent"], _about_about_component__WEBPACK_IMPORTED_MODULE_9__["AboutComponent"], _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_10__["ContactUsComponent"], _contact_form_contact_form_component__WEBPACK_IMPORTED_MODULE_11__["ContactFormComponent"]]
        })
    ], StaticPagesModule);
    return StaticPagesModule;
}());



/***/ }),

/***/ "./src/app/static-pages/static-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/static-pages/static-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: StaticRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaticRoutingModule", function() { return StaticRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../team-list.resolver */ "./src/app/team-list.resolver.ts");
/* harmony import */ var _return_policy_return_policy_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./return-policy/return-policy.component */ "./src/app/static-pages/return-policy/return-policy.component.ts");
/* harmony import */ var _faq_faq_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./faq/faq.component */ "./src/app/static-pages/faq/faq.component.ts");
/* harmony import */ var _terms_use_terms_use_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./terms-use/terms-use.component */ "./src/app/static-pages/terms-use/terms-use.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./about/about.component */ "./src/app/static-pages/about/about.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/static-pages/contact-us/contact-us.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var staticRoutes = [
    {
        path: 'return-policy',
        component: _return_policy_return_policy_component__WEBPACK_IMPORTED_MODULE_3__["ReturnPolicyComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    },
    {
        path: 'faq',
        component: _faq_faq_component__WEBPACK_IMPORTED_MODULE_4__["FaqComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    },
    {
        path: 'terms',
        component: _terms_use_terms_use_component__WEBPACK_IMPORTED_MODULE_5__["TermsUseComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    },
    {
        path: 'about-us',
        component: _about_about_component__WEBPACK_IMPORTED_MODULE_6__["AboutComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    },
    {
        path: 'contact-us',
        component: _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_7__["ContactUsComponent"],
        resolve: { teams: _team_list_resolver__WEBPACK_IMPORTED_MODULE_2__["TeamListResolver"] }
    },
];
var StaticRoutingModule = /** @class */ (function () {
    function StaticRoutingModule() {
    }
    StaticRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(staticRoutes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], StaticRoutingModule);
    return StaticRoutingModule;
}());



/***/ }),

/***/ "./src/app/static-pages/terms-use/terms-use.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/static-pages/terms-use/terms-use.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"page-wrap mb-5\">\r\n   <div class=\"container\">\r\n\t    <div class=\"row\">\r\n\t      <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t<ul class=\"breadcrumb\">\r\n\t\t\t  \t<li class=\"breadcrumb-item\"><a routerLink=\"/\">Home</a></li>\r\n\t\t\t  \t<li class=\"breadcrumb-item active\">Terms of Use</li>\r\n\t\t\t</ul>\r\n\t      </div>\r\n\t    </div>\r\n\t    <div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t      \t\t<h1 class=\"page-head text-center mb-3\">Terms of Use</h1>\r\n\t      \t</div>\r\n\t    </div>\r\n\t    <div class=\"row\">\r\n\t      \t<div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t\t\t    <div class=\"term-use-wrap\">\r\n\t\t\t  \t\t<h2>General information</h2>\r\n\t\t\t\t  \t<div class=\"term-use-details\">\r\n\t\t\t\t  \t\t<p>Welcome to our Privacy Policy page! When you use our store services, you trust us with your information. This Privacy Policy is meant to help you understand what data we collect, why we collect it, and what we do with it. When you share information with us, we can make our services even better for you. For instance, we can show you more relevant search results and ads, help you connect with people or to make sharing with others quicker and easier. As you use our services, we want you to be clear how we’re using information and the ways in which you can protect your privacy. This is important; we hope you will take time to read it carefully. Remember, you can find controls to manage your information and protect your privacy and security. We’ve tried to keep it as simple as possible.</p>\r\n\t\t\t\t  \t</div>\r\n\t\t\t    </div>\r\n\t\t\t    <div class=\"term-use-wrap\">\r\n\t\t\t  \t\t<h2>Right to access, correct and delete data and to object to data processing</h2>\r\n\t\t\t\t  \t<div class=\"term-use-details\">\r\n\t\t\t\t  \t\t<p>Our customers have the right to access, correct and delete personal data relating to them, and to object to the processing of such data, by addressing a written request, at any time. The Company makes every effort to put in place suitable precautions to safeguard the security and privacy of personal data, and to prevent it from being altered, corrupted, destroyed or accessed by unauthorized third parties. However, the Company does not control each and every risk related to the use of the Internet, and therefore warns the Site users of the potential risks involved in the functioning and use of the Internet. The Site may include links to other web sites or other internet sources. As the Company cannot control these web sites and external sources, the Company cannot be held responsible for the provision or display of these web sites and external sources, and may not be held liable for the content, advertising, products, services or any other material available on or from these web sites or external sources.</p>\r\n\t\t\t\t  \t</div>\r\n\t\t\t    </div>\r\n\t\t\t    <div class=\"term-use-wrap\">\r\n\t\t\t  \t\t<h2>Management of personal data</h2>\r\n\t\t\t\t  \t<div class=\"term-use-details\">\r\n\t\t\t\t  \t\t<p>You can view or edit your personal data online for many of our services. You can also make choices about our collection and use of your data. How you can access or control your personal data will depend on which services you use. You can choose whether you wish to receive promotional communications from our store by email, SMS, physical mail, and telephone. If you receive promotional email or SMS messages from us and would like to opt out, you can do so by following the directions in that message. You can also make choices about the receipt of promotional email, telephone calls, and postal mail by visiting and signing into Company Promotional Communications Manager, which allows you to update contact information, manage contact preferences, opt out of email subscriptions, and choose whether to share your contact information with our partners. These choices do not apply to mandatory service communications that are part of certain store services.</p>\r\n\t\t\t\t  \t</div>\r\n\t\t\t    </div>\r\n\t\t\t    <div class=\"term-use-wrap\">\r\n\t\t\t  \t\t<h2>Information we collect</h2>\r\n\t\t\t\t  \t<div class=\"term-use-details\">\r\n\t\t\t\t  \t\t<p>Our store collects data to operate effectively and provide you the best experiences with our services. You provide some of this data directly, such as when you create a personal account. We get some of it by recording how you interact with our services by, for example, using technologies like cookies, and receiving error reports or usage data from software running on your device. We also obtain data from third parties (including other companies). For example, we supplement the data we collect by purchasing demographic data from other companies. We also use services from other companies to help us determine a location based on your IP address in order to customize certain services to your location. The data we collect depends on the services and features you use.</p>\r\n\t\t\t\t  \t</div>\r\n\t\t\t    </div>\r\n\t\t\t    <div class=\"term-use-wrap\">\r\n\t\t\t  \t\t<h2>How we use your information</h2>\r\n\t\t\t\t  \t<div class=\"term-use-details\">\r\n\t\t\t\t  \t\t<p>Our store uses the data we collect for three basic purposes: to operate our business and provide (including improving and personalizing) the services we offer, to send communications, including promotional communications, and to display advertising. In carrying out these purposes, we combine data we collect through the various store services you use to give you a more seamless, consistent and personalized experience. However, to enhance privacy, we have built in technological and procedural safeguards designed to prevent certain data combinations. For example, we store data we collect from you when you are unauthenticated (not signed in) separately from any account information that directly identifies you, such as your name, email address or phone number.</p>\r\n\t\t\t\t  \t</div>\r\n\t\t\t    </div>\r\n\t\t\t    <div class=\"term-use-wrap\">\r\n\t\t\t  \t\t<h2>Sharing your information</h2>\r\n\t\t\t\t  \t<div class=\"term-use-details\">\r\n\t\t\t\t  \t\t<p>We share your personal data with your consent or as necessary to complete any transaction or provide any service you have requested or authorized. For example, we share your content with third parties when you tell us to do so. When you provide payment data to make a purchase, we will share payment data with banks and other entities that process payment transactions or provide other financial services, and for fraud prevention and credit risk reduction. In addition, we share personal data among our controlled affiliates and subsidiaries. We also share personal data with vendors or agents working on our behalf for the purposes described in this statement. For example, companies we've hired to provide customer service support or assist in protecting and securing our systems and services may need access to personal data in order to provide those functions. In such cases, these companies must abide by our data privacy and security requirements and are not allowed to use personal data they receive from us for any other purpose. We may also disclose personal data as part of a corporate transaction such as a merger or sale of assets.</p>\r\n\t\t\t\t  \t</div>\r\n\t\t\t    </div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n    </div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/static-pages/terms-use/terms-use.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/static-pages/terms-use/terms-use.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".breadcrumb {\n  background-color: transparent;\n  margin-top: 0;\n  margin-bottom: 0;\n  padding: 0; }\n  .breadcrumb a {\n    font-size: 14px;\n    font-weight: 400;\n    color: #666;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .breadcrumb .active {\n    font-size: 14px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif; }\n  .term-use-wrap {\n  margin-bottom: 40px; }\n  .term-use-wrap h2 {\n    font-size: 18px;\n    font-weight: 700;\n    color: #000;\n    font-family: \"Libre Baskerville\", serif;\n    margin-bottom: 20px;\n    line-height: 24px; }\n  .term-use-wrap .term-use-details p {\n    font-size: 14px;\n    font-weight: 400;\n    color: #000;\n    font-family: \"Libre Franklin\", sans-serif;\n    line-height: 24px;\n    letter-spacing: .2px; }\n"

/***/ }),

/***/ "./src/app/static-pages/terms-use/terms-use.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/static-pages/terms-use/terms-use.component.ts ***!
  \***************************************************************/
/*! exports provided: TermsUseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermsUseComponent", function() { return TermsUseComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TermsUseComponent = /** @class */ (function () {
    function TermsUseComponent() {
    }
    TermsUseComponent.prototype.ngOnInit = function () {
    };
    TermsUseComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-terms-use',
            template: __webpack_require__(/*! ./terms-use.component.html */ "./src/app/static-pages/terms-use/terms-use.component.html"),
            styles: [__webpack_require__(/*! ./terms-use.component.scss */ "./src/app/static-pages/terms-use/terms-use.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TermsUseComponent);
    return TermsUseComponent;
}());



/***/ }),

/***/ "./src/app/team-list.resolver.ts":
/*!***************************************!*\
  !*** ./src/app/team-list.resolver.ts ***!
  \***************************************/
/*! exports provided: TeamListResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamListResolver", function() { return TeamListResolver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TeamListResolver = /** @class */ (function () {
    function TeamListResolver() {
    }
    TeamListResolver.prototype.getTeams = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(['Barcelona', 'Real Madrid', 'Toulouse', 'PSG']).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["delay"])(1000));
    };
    TeamListResolver.prototype.resolve = function (route, state) {
        return this.getTeams();
    };
    TeamListResolver = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], TeamListResolver);
    return TeamListResolver;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Shree\Desktop\myProjects\zendeskthemes\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map